from scipy.fft import rfft, rfftfreq, irfft
import numpy as np
import re

from walkthrough_eeghelpers import MNEDataset

m = MNEDataset(batch_step=1/120,batch_window=1/40)
channels = m.channels

#channel selection
channel = 23

label = np.zeros(len(channels))

EEG_L = ["FP1", "F7", "F3", "A1", "T3", "C3", "T5", "P3", "O1"]
EEG_R = ["FP2", "F8", "F4", "A2", "T4", "C4", "T6", "P4", "O2"]
EEG_M = ["NZ", "FPZ", "FZ", "CZ", "PZ", "OZ"]
#binmagnitude={}  #dict
#binmagnitude[0]=0
#binmagnitude[0]+=0.2
#binmagnitude[0.5]=0.2
#binmagnitude["SOMEREFERENCE"]=0.2


EEG_Labels = [EEG_L, EEG_R, EEG_M]

for n in np.arange(0, len(channels)):
    for labels in EEG_L:
        if re.compile(labels).search(channels[n]):
            label[n] = 1
    for labels in EEG_R:
        if re.compile(labels).search(channels[n]):
            label[n] = 1
    for labels in EEG_M:
        if re.compile(labels).search(channels[n]):
            label[n] = 1
    if re.compile("EKG").search(channels[n]):
        label[n] = 0
    elif label[n] != 1:
        label[n] = 2
print(label)

data,times,last=m.get_next_batch()
freq=1/times[1]
period = times[1]
fft_freq = rfftfreq(len(data[channel]), period)

# digitise into specified number of bins
bintotal = 100
bins=np.linspace(0,freq/2,bintotal)
fft_1_bins=np.digitize(fft_freq,bins)

# kept together
fft_1_total = []
fft_1_bintotal = 0


counter=0
vector = []

while(not last):
    data, times, last = m.get_next_batch()
    counter+=1

    fft_1 = rfft(data)
    bin = 0
    count = 0
    for n in fft_1_bins:
        if n == bin:
            fft_1_bintotal = fft_1_bintotal + fft_1[channel, n]
            count += 1
        else:
            if count != 0:
                fft_1_total.append(fft_1_bintotal / count)
            else:
                fft_1_total.append(fft_1_bintotal)
            fft_1_bintotal = 0
            count = 0
            bin += 1

    #fft_1_total = np.array(fft_1_total)

    # recover signal from amplitude/phase data in fft_1_total (already in real + imaginary)
    recover = irfft(fft_1_total)

    # fft_1_mag_norm = normalize(fft_1_mag)

    x = [np.absolute(fft_1_total), np.angle(fft_1_total)] # label[channel]] #channel_labelled as EEG = 1; EKG = 0; neither = 2
    # y_true=
    # y_pred=
    # new_vector = np.array(new_vector, dtype=float).T
    # vector.append(new_vector)
    ### convert from numpy to list
    ## json.dump() dumps save it as astring
    ## To load. Load list - >np.array(xxx)


    if last:
        print(f"{times[-1]/60}s")
        print(f"{counter} steps")

vector = np.array(vector, dtype=object)
print(vector.shape)