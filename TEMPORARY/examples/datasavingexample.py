from eeghelpers import *

if __name__ == '__main__':

	dm = DataManager()
	folder = os.path.abspath("test")

	#now generate a datasets to save. Often we have more data than we can fit into memory so we have to do chunks of it.
	saveevery=1000

	for i in range(1000):
		X=np.random.uniform(0.0,1.0,1000)  #choose 1000 random numbers between 0 and 100,000,000
		Y_true=np.square(X)

		dm.saveData(X, Y_true, folder,n_per_file=500)

	print(f"Done")