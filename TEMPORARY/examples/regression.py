import matplotlib.pyplot as plt
import numpy as np

import random

#small change
def generateStraightLineDataset(n=10000, min_x=0, max_x=100, seed=None):
	random.seed(seed)  #if you use the same seed you will get the rnd values.
	x_vals=np.linspace(min_x,max_x,num=n)
	y_vals = 0.5 * x_vals + 2
	errs = np.random.normal(0, 3, y_vals.shape)
	y_vals=y_vals+errs

	return x_vals,y_vals
def best_fit(X, Y):
	# fit y=mx+c Least squares
	xbar = sum(X)/len(X)
	ybar = sum(Y)/len(Y)
	n = len(X) # or len(Y)
	numer = sum([xi*yi for xi,yi in zip(X, Y)]) - n * xbar * ybar
	denum = sum([xi**2 for xi in X]) - n * xbar**2

	m = numer / denum
	c = ybar - m * xbar
	print('best fit line:\ny = {:.2f} + {:.2f}x'.format(m, c))

	return m, c

def leastSquares(x_true,y_true,plot=False):

	m, c = best_fit(x_true, y_true)  #fit y=mx+c
	x=np.arange(0,100)
	y=m*x+c
	if plot:
		plt.plot(x,y)
		plt.legend(["Data",f"y={m}x+{c}"])
		plt.show()
	return m,c

def generatePolyDataset(n=10000, min_x=0, max_x=100, seed=None,plot=False):
	random.seed(seed)  #if you use the same seed you will get the rnd values.
	x_vals=np.linspace(min_x,max_x,num=n)
	#errs=2*np.random.random_sample(len(x_vals))
	##create error in the data so that it is not perfect.
	#vals=x_vals+errs
	#y_vals=gnd_truth(vals)
	y_vals = 0.5 * np.power(x_vals,2)+3*x_vals + 2
	#errs=3*np.random.random_sample(len(x_vals))
	errs = np.random.normal(0, 3, y_vals.shape)
	y_vals=y_vals+errs
	if plot:
		plt.plot(x_vals,y_vals)
		plt.legend(["Data"])
		plt.show()
	return x_vals,y_vals

def func(x, a, b, c):
    return a + b*x + c*x*x

if __name__ == '__main__':
	####
	# 1. First we create some toy data in this case F(x)=mx+c
	x_true,y_true=generateStraightLineDataset() #this is our dataset
	#the question is what can we build a system to predict y given a value of x?
	plt.plot(x_true,y_true)
	####
	# 2. Make an assumption about the model - in this case y=mx+c and then use various methods to determine the parameters m and c.
	m,c=leastSquares(x_true,y_true,plot=False)

	###############
	# Achieving the same above using sci-kit learn
	# https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html
	from sklearn import linear_model
	###LinearRegression fits a linear model with coefficients w = (w1, …, wp) to minimize the residual sum of squares between the observed targets in the dataset, and the targets predicted by the linear approximation.
	#shape the data as required by sklearn
	x_true=np.reshape(x_true,newshape=(-1,1))
	#what model will I try to fit, a model with coefficients w1,...wp?
	reg = linear_model.LinearRegression(fit_intercept=True)
	reg.fit(x_true,y_true)
	print(f"Linear Regression Y={reg.coef_[0]}x +{reg.intercept_}")
	###Using ridge regression
	reg = linear_model.Ridge(alpha=.5)
	reg.fit(x_true,y_true)
	print(f"Ridge Regression Y={reg.coef_[0]}x +{reg.intercept_}")
	###Using lasso regression
	reg = linear_model.Lasso(alpha=0.1)
	reg.fit(x_true,y_true)
	print(f"Lasso Regression Y={reg.coef_[0]}x +{reg.intercept_}")

	###Using lasso regression
	reg = linear_model.LassoLars(alpha=.1, normalize=False)
	reg.fit(x_true,y_true)
	print(f"LassoLars Regression Y={reg.coef_[0]}x +{reg.intercept_}")

	####  Polynomial
	# Optimize some model function.
	import scipy.optimize as optimization
	x_true,y_true=generatePolyDataset(plot=False)
	x0 = np.zeros(3)   #starting values for the parameters
	val=optimization.curve_fit(func, x_true, y_true)
	print(f"Fn Optimisation Y={val[0][2]}x^2 +{val[0][1]}x+{val[0][0]}")
	###Numpy poly fit.
	coefs = np.polyfit(x_true, y_true, 2)
	print(f"Fn NP Polyfit Y={coefs[0]}x^2 +{coefs[1]}x+{coefs[2]}")

	###Baysian
	#reg = linear_model.BayesianRidge()
	#x_true=np.reshape(x_true,newshape=(-1,1))
	#reg.fit(x_true,y_true)
	#print(f"Bayesian Regression {reg.coef_[0]}, {reg.intercept_}")




	print("Done")


