import mne
from PIL import Image
from scipy.fft import rfft, rfftfreq, irfft
import json_tricks    #pip install json-tricks  #this handles numpy and additional datatypes which standard json does not.
from utils import *
import glob
import os
import random

class MNEDataset:
	def __init__(self, batch_step=.5, batch_window=1, samplerate = None, lastEDFAction=0, lastSampleAction=0,basefolder="./EDFs"):
		"""
		:param batch_step:
		:param batch_window:
		:param samplerate:
		:param lastEDFAction: 0: quit. 1. looparound.
		:param lastSampleAction: 0: loadNextEdf. 1. looparound.
		"""
		self.basefolder = basefolder
		self.lastEDFAction=lastEDFAction
		self.lastSampleAction=lastSampleAction
		self.initMNE(batch_step=batch_step,batch_window=batch_window,samplerate = samplerate)

	def initMNE(self,batch_step=.5,batch_window=1,edf_idx=None,samplerate = None):
		print(f"{self.basefolder}")
		self.edffiles = glob.glob(os.path.join(self.basefolder, "*.edf"))
		if edf_idx==None:
			self.edf_idx=-1
		else:
			self.edf_idx=edf_idx-1  ##-1 because next_edf adds 1
		if samplerate!=None: self.samplerate=samplerate
		self.next_edf()
		#data = mne.io.read_raw_edf(edfs[self.edf_idx])
		#raw_data = data.get_data()
		# you can get the metadata included in the file and a list of all channels:
		#self.edffiles = edfs
		#self.edfdata = data
		#self.rawdata = raw_data[3]  # TODO: remove this index if you want more channels. Randomly chose 3 - no reason just wanted 1 channel
		#self.meanEEGPower = np.mean(self.rawdata)
		#self.maxpower = np.max(self.rawdata)
		#self.minpower = np.min(self.rawdata)
		self.batch_step = batch_step  # this how much we increment step in minutes.
		self.batch_window=batch_window #this is how wide the window is in minutes.
		self.idx=0 #this is how many time steps have been completed.

	def init_edf(self,samplerate=None):
		#these are the things that might be different with each EDF.
		#call everytime the edf changes
		self.last_sample=self.edfdata.last_samp
		self.info = self.edfdata.info  # https://mne.tools/stable/generated/mne.io.read_raw_edf.html
		self.channels = self.edfdata.ch_names
		raw_data, times = self.edfdata.get_data(start=0, stop=10, return_times=True) #get 10 samples
		est_samplerate=1/((times[-1]--times[0])/9)
		est_samplerate2=1/(times[1]-times[0])
		if samplerate!=None:
			self.samplerate=samplerate
		else:
			try:
				self.samplerate =  self.info['sfreq']
			except:
				self.samplerate=est_samplerate2
		if est_samplerate2!=est_samplerate or est_samplerate!=self.samplerate:
			if samplerate != None:
				self.samplerate = samplerate
			else:
				self.samplerate=est_samplerate2
			print(f"SAMPLE RATE CONFLICT IN {self.current_edf()} line 50 eeghelpers.py. {self.samplerate}Hz set.")
		# info = mne.create_info(ch_names=channels, sfreq=sr,ch_types="eeg").set_montage(montage)
		# https://www.researchgate.net/profile/Heidelise_Als/publication/228064755/figure/fig1/AS:203120069091344@1425439006780/Standard-EEG-electrode-names-and-positions-Head-in-vertex-view-nose-above-left-ear-to.png
		# data.plot()
		self.layout = mne.channels.read_layout('biosemi')
		self.rawtimes = self.edfdata.times ##all the timestepsp in this file

	def next_edf(self,set_idx=0):
		self.edf_idx+=1
		if self.edf_idx>=len(self.edffiles):
			print(f"FINISHED  last EDF ")
			if self.lastEDFAction==0:
				exit(0)
			elif self.lastEDFAction==1:
				self.edf_idx=0
				print("LOOPING AROUND")
		self.edfdata = mne.io.read_raw_edf(self.edffiles[self.edf_idx])
		self.idx=set_idx ###reset the natch index
		self.init_edf()
		return self.edffiles[self.edf_idx],self.edf_idx

	def current_edf(self):
		return self.edffiles[self.edf_idx],self.edf_idx

	def get_next_batch(self,samplen=None):
		#gets the next time step of data from the EEG.
		start_time=self.idx*self.batch_step
		duration=self.batch_window
		data,times,last=self.get_data_time(start_time,duration,samplen)
		if last:
			if self.lastSampleAction==0:

				self.next_edf()

			elif self.lastSampleAction==1:
				self.idx=0

		else:	self.idx=self.idx+1

		return data,times,last

	def get_data_time(self,start_time_mins,duration_mins,samplen=None):
		sample_S=int(60*start_time_mins*self.samplerate)
		if samplen!=None:
			sample_finish=sample_S+samplen
		else:
			sample_finish=int(np.ceil(60*(start_time_mins+duration_mins)*self.samplerate))
		last=False
		if sample_finish>self.last_sample:
			raw_data,times = self.edfdata.get_data(start=sample_S,stop=self.last_sample,return_times=True)
			last = True
		else:
			#raw_data,times = self.edfdata.get_data_samples(start=sample_S,stop=sample_finish,return_times=True)
			raw_data,times = self.edfdata.get_data(start=sample_S,stop=sample_finish,return_times=True)

		return raw_data,times,last

	def get_data_samples(self,start,finish,return_times=True):
		raw_data,times = self.edfdata.get_data(start=start,stop=finish,return_times=return_times)
		return raw_data,times

	def get_fft(self,data,nbins=None):
		#period=1/self.samplerate
		if nbins==None:
			fft_vals = rfft(data)
			fft_freqs = rfftfreq(len(data[0]), 1/self.samplerate) #freqs are tge same for each channel
		else:
			fft_vals = rfft(data,n=nbins*2)
			fft_freqs = rfftfreq(nbins*2, 1/self.samplerate) #freqs are tge same for each channel
		return fft_vals,fft_freqs

	def get_ifft(self,data,n=None):
		"""
		:param data: The FFT data
		:param n: number of bins
		:return: the ifft with the given number of bins
		"""
		if n==None: n=len(data[0])*2+1
		ifft = irfft(data,n)

		return ifft

	def cosreconstrction(self, y_time, fft_vals, fft_freqs, channel):
		##This reconstructs the signal from the frequency and phase components INSTEAD OF IFFT.
		##z=r(cos(phi)+isin(phi)
		#cos is the real component, r is mag and phi is the angle
		##why do this?? when you do an ifft for a reduced number of bins it will produce a smaller time period gving the impression that the frequency binning is not working properly.
		#in actual fact most of the information still exists in the frequency domain with less bins - despite the IFFT function failing to reproduce the exact function - by reconstructing with cos we can get a reconstruction for any time length.

		vals=fft_vals[channel]
		mag = np.absolute(vals)/len(fft_freqs) #todo = why /len(fft_freqs) here?
		ts=y_time #todo = y_time never defined? it is just times from get data samples?
		#zeros=np.where(mag==0)
		phase = np.angle(vals)
		#phase[zeros]=0.0
		ws = 2. * np.pi * fft_freqs # todo this normalises phase?

		#mag[mag<1]=0.0
		k=int(.2*len(mag)) #zero out the bottom 20% of values
		idx = np.argpartition(mag, k)
		smallestidx=idx[:k]
		mag[smallestidx]=0

		fn=np.zeros_like(ts)
		for i in range(len(fft_freqs)):
			f = fft_freqs[i]
			if mag[i]!=0:
				#print(mag[i])
				a=9.96*np.sin(62.83*ts)
				z=mag[i]*np.complex(np.cos(phase[i]),np.sin(phase[i]))

			#ws=2. * np.pi * f
			fn=fn+mag[i] * np.cos(ws[i] * ts +phase[i])

			#fn=fn+mag[i] * (np.sin(ws[i] * ts)



		return fn

	def movingaverage(self,interval, window_size):
		window = np.ones(int(window_size)) / float(window_size)

		val= np.convolve(interval, window, 'same')
		return val

	def plot_recon(self, orig_sig, times,channel=23, nbins=125,mvavpts=10):

		fft_vals, fft_freqs = self.get_fft(orig_sig,nbins=nbins) #
		recon=self.cosreconstrction(times, fft_vals, fft_freqs, channel=channel)
		#plt.ioff()
		plt.figure(1)
		orig_chan=orig_sig[channel]
		plt.subplot(3, 1, 1)
		plt.plot(times, orig_chan)
		plt.plot(times[:len(recon)], recon[:len(times)])
		plt.xlabel('Time (s)')
		plt.ylabel('Amp')
		plt.title(f'EEG CH{channel}')
		######################################################
		orig_fft_vals, orig_fft_freqs = self.get_fft(orig_sig) #
		recon_fft_vals, recon_fft_freqs = self.get_fft([recon],nbins=nbins) #
		plt.subplot(3, 1, 2)
		origabs=np.absolute(orig_fft_vals[channel])
		origabs=origabs/np.max(origabs)
		(markerline, stemlines, baseline)=plt.stem(orig_fft_freqs, origabs,use_line_collection=True, markerfmt='.',label=f"Orig {len(origabs)} bins")
		plt.setp(baseline, visible=False)
		plt.setp(stemlines, 'color', plt.getp(markerline, 'color'))
		plt.setp(stemlines, 'linestyle', 'dashdot')
		plt.setp(markerline, 'markersize', '7')
		reconabs=np.absolute(recon_fft_vals[0])
		reconabs=reconabs/np.max(reconabs)
		(markerline, stemlines, baseline)=plt.stem(recon_fft_freqs, reconabs,use_line_collection=True, markerfmt='.',label=f"Recon {len(reconabs)} bins")
		plt.setp(baseline, visible=False)
		plt.setp(stemlines, 'color', plt.getp(markerline, 'color'))
		plt.setp(stemlines, 'linestyle', 'dotted')
		plt.setp(markerline, 'markersize', '6')
		plt.xlabel('Freq (Hz)')
		plt.ylabel('Mag')
		plt.legend(loc="best", prop={'size': 6})

		######################################################
		ax=plt.subplot(3, 1, 3)
		delta=recon[:len(orig_chan)]-orig_chan[:len(recon)]
		mvav=self.movingaverage(delta,mvavpts)
		(markerline, stemlines, baseline) = plt.stem(times[:len(delta)], delta[:len(times)],use_line_collection=True, markerfmt='.',label="Diff")
		plt.plot(times,mvav,label=f"MAv {mvavpts}p")
		plt.setp(baseline, visible=False)
		plt.setp(stemlines, 'color', plt.getp(markerline, 'color'))
		plt.setp(stemlines, 'linestyle', 'dotted')
		plt.setp(markerline, 'markersize', '6')
		plt.xlabel('Time (s)')
		plt.ylabel('diff Amp')
		plt.legend(loc="best", prop={'size': 6})
		av=np.average(delta)
		sum=np.sum(delta)

		print(f"Time Error Sum:{sum}, Av:{av}")
		ax.text(0,0,f"ERR\n Sum:{np.round(sum,1)}\n Av:{np.round(av,1)}", fontsize=7,bbox={'facecolor': 'red', 'alpha': 0.05, 'pad': 2})
		plt.tight_layout()

		#plt.show()
		######################################################
		plt.figure(2)
		plt.subplot(3, 1, 1)
		plt.plot(times, orig_chan)
		plt.plot(times[:len(recon)], recon[:len(times)])

		plt.xlabel('Time (s)')
		plt.ylabel('Amp')
		plt.title(f'EEG CH{channel}')
		######################################################
		plt.subplot(3, 1, 2)
		#plt.plot(fft_freqs, np.angle(fft_vals[channel]))
		#plt.plot(recon_fft_freqs, np.angle(recon_fft_vals[channel]))
		(markerline, stemlines, baseline)=plt.stem(fft_freqs, np.angle(fft_vals[channel]),use_line_collection=True, markerfmt='.')
		plt.setp(baseline, visible=False)
		plt.setp(stemlines, 'color', plt.getp(markerline, 'color'))
		plt.setp(stemlines, 'linestyle', 'dotted')
		plt.setp(markerline, 'markersize', '7')
		(markerline, stemlines, baseline)=plt.stem(recon_fft_freqs, np.angle(recon_fft_vals[0]),use_line_collection=True, markerfmt='.')
		plt.setp(baseline, visible=False)
		plt.setp(stemlines, 'color', plt.getp(markerline, 'color'))
		plt.setp(stemlines, 'linestyle', 'dotted')
		plt.setp(markerline, 'markersize', '6')

		plt.xlabel('Freq (Hz)')
		plt.ylabel('Phase')
		######################################################
		plt.subplot(3, 1, 3)
		delta=np.angle(recon_fft_vals[0])[:len(fft_vals[channel])]-np.angle(fft_vals[channel])[:len(recon_fft_vals[0])]
		#plt.plot(recon_fft_freqs[:len(delta)], delta[:len(recon_fft_freqs)])
		mvav=self.movingaverage(delta,mvavpts)
		(markerline, stemlines, baseline) = plt.stem(recon_fft_freqs[:len(delta)], delta[:len(recon_fft_freqs)],
													 use_line_collection=True, markerfmt='.',label="Diff")
		plt.plot(recon_fft_freqs, mvav, label=f"MAv {mvavpts}p")
		plt.setp(baseline, visible=False)
		plt.setp(stemlines, 'color', plt.getp(markerline, 'color'))
		plt.setp(stemlines, 'linestyle', 'dotted')
		plt.setp(markerline, 'markersize', '6')
		plt.legend(loc="best", prop={'size': 6})
		plt.xlabel('Freq (s)')
		plt.ylabel('diff Phase')
		av=np.average(delta)
		sum=np.sum(delta)
		print(f"Phase Error Sum:{sum}, Av:{av}")
		plt.text(0,0,f"ERR\n Sum:{np.round(sum,1)}\n Av:{np.round(av,1)}", fontsize=7,bbox={'facecolor': 'red', 'alpha': 0.05, 'pad': 2})
		plt.tight_layout()
		plt.show()

		##get mag and phase for the specific channel
		#plt.figure(2)
		freqs=[]
		mags=[]
		phases=[]
		# for key,val in mphasedic.items():
		# 	if key==0: continue #skip the dc component
		# 	freqs.append(key)
		# 	mag,phase=val[channel]
		# 	mags.append(mag)
		# 	phases.append(phase)
		# plt.subplot(4, 1, 2)
		# plt.plot(freqs, mags)
		# plt.xlabel('Freq (Hz)')
		# plt.ylabel('Magnitude')
		# plt.subplot(4, 1, 3)
		# plt.plot(freqs, phases)
		# plt.xlabel('Freq (Hz)')
		# plt.ylabel('Phase')
		# #plt.title(f'EEG CH{channel}')

		plt.show()
		print("finished")

class DataManager():
	def __init__(self,folder,testsplit=.2,batchsize=600,datapoolsize=1200):
		#testsplit is the proportion of data to hold back as the test set.
		#note that it does not hold back exactly the data instead it simply holds back that proportion of files.
		#There could be issues with correlation of data if your files are highly correlated.
		#
		self.testsplit=0.2
		self.folder=folder
		print(f"getting data from {folder}")
		os.makedirs(folder, exist_ok=True)
		self.filenames = glob.glob(f"{folder}/*.json")  # get current file names.
		n=int(testsplit*len(self.filenames))
		idxes=np.arange(0,len(self.filenames))
		testidxes=np.random.choice(idxes,n,replace=False)
		self.testfiles=list(np.array(self.filenames)[testidxes])
		self.filenames=list(np.delete(self.filenames, testidxes))

		self.filenamesremaining=[]  #as files are loaded they are removed from this list so we dont load them twice in one epoch

		self.batchsize=batchsize
		self.datapoolsize=datapoolsize #this is a multiple of how many more data points to load to reduce correlation of data
		self.epochstarted=False #is True after the first batch is done.
		self.storedX=[]
		self.storedY=[]
		self.lastfile=False
		self.batchcounter=0  #a counter that keeps track of how many batches have been done
		self.epochcounter=0
		self.testset_started=False
	def saveData(self,X_true,Y_true,n_per_file=10,randomise=True):
		"""
		Saves ground truth data conveniently.
		:param X_true,Y_true: X_true is an n-D numpy array/tensor. Y-true is an n vector representing the ground truth value for X-true
		:param filename: The folder to save the data
		:param n_per_file=10: how many samples to save per file. Using 1 means that each example will be in its own file.
				A smaller number is efficient when randomising data to train but will be slower to load.
				Larger numbers will be more efficient to load, but may mean that randomisations are not truly random cause the same n datapoints may always occur together.
				If all the data fits into memory at once then you can overcome these points. If however you need to load data then the smaller the n the better.
		:param randomise=True: this randomises the data before saving - it will decrease the chance of data being correlated within the same file.
		:return:
		"""

		filenames=glob.glob(f"{self.folder}/*.json") 		#get current file names.
		data=list(zip(X_true,Y_true))
		if randomise:
			random.shuffle(data)
		nums=[get_file_nums(os.path.basename(strg))[0] for strg in filenames]
		if len(nums)==0: #this sets the next value for the filename
			idx=-1
		else:
			idx=max(nums)

		datachunks=chunks(data,n_per_file,trim=False)
		for d in datachunks:
			idx+=1
			X, Y=list(zip(*d))
			filename=os.path.join(self.folder,f"{idx}.json")
			json_tricks.dump([X,Y],filename)

	def get_testset_batch(self,batchsize=None):
		#loads a batch from the testset from the disk
		if batchsize==None: # in case you want more data at a time.
			batchsize=self.batchsize
		###gets one batch of data.
		if not self.testset_started:
			self.testset_started=True
			self.testfilesremaining = self.testfiles.copy()  # as files are loaded they are removed from this list so we dont load them twice in one epoch
			random.shuffle(self.testfilesremaining) #todo: can reduce memory size by removing the folder from this list and adding it back when loading. Also not sure if I care about shuffling the test set. it means each repeat a different amount will be given

		outoffilesflag=False
		finished=False
		batchx,batchy=[],[]
		while len(batchx)<batchsize: ##then need to add more data to the pool
			if len(self.testfilesremaining) == 0:
				outoffilesflag=True
				break
			filename = self.testfilesremaining.pop(-1)
			X,Y=json_tricks.load(filename)
			batchx.extend(X)
			batchy.extend(Y)
		if outoffilesflag:
			if len(batchx)<batchsize: ##then we are out of files to load and don't have enough data for 1 full batch.
				finished=True   #we will return whatever we can but indicate that this epoch is finished.
				self.testset_started=False #this is so the next time this fn is called we start a new epoch


		X = np.array(batchx)
		Y = np.array(batchy)


		return X,Y,finished

	def get_batch(self):
		self.batchcounter+=1
		###gets one batch of data.
		if not self.epochstarted:
			self.epochstarted=True
			#self.filenames = glob.glob(f"{self.folder}/*.json")  # if you want to generate data while you are learning then you could uncomment this, however your test set data may be stuffed up.
			self.filenamesremaining = self.filenames.copy()  # as files are loaded they are removed from this list so we dont load them twice in one epoch
			random.shuffle(self.filenamesremaining) #todo: can reduce memory size by removing the folder from this list
			self.batchcounter=0

		outoffilesflag=False
		finished=False
		while len(self.storedX)<self.datapoolsize: ##then need to add more data to the pool
			if len(self.filenamesremaining) == 0:
				outoffilesflag=True
				break
			filename = self.filenamesremaining.pop(-1)
			X,Y=json_tricks.load(filename)
			self.storedX.extend(X)
			self.storedY.extend(Y)
		if outoffilesflag:
			if len(self.storedX)<self.batchsize: ##then we are out of files to load and don't have enough data for 1 full batch.
				finished=True   #we will return whatever we can but indicate that this epoch is finished.
				self.epochstarted=False #this is so the next time this fn is called we start a new epoch
				self.epochcounter += 1

		idxes=np.arange(0,len(self.storedX))
		batchidxes=np.random.choice(idxes,min(self.batchsize,len(self.storedX)),replace=False)

		X = np.array(self.storedX)[batchidxes]
		Y = np.array(self.storedY)[batchidxes]
		self.storedX=list(np.delete(self.storedX, batchidxes,axis=0))
		self.storedY=list(np.delete(self.storedY, batchidxes,axis=0))

		return X,Y,finished

if __name__ == '__main__':
	m = MNEDataset(batch_step=.25, batch_window=.5)
	dm = DataManager

	X_true=[1,2,3,4]
	Y_true=[2,3,9,16]
	folder=os.path.abspath("test")

	dm.saveData(X_true,Y_true,folder)

	data, times, last = m.get_next_batch()
	freq = 1 / times[1]

	###Calling functions directly
	showdata_s, time_s = m.get_data_samples(0, m.samplerate)  # this should be 1 seconds worth of samples
	showdata_t, time2_t, last = m.get_data_time(0, 1 / 60)  # this is 1 second worth o samples should be same as above

	###Cycling through the data.
	counter = 0
	while (not last):
		data, times, last = m.get_next_batch()
		counter += 1
		if last:
			print(f"{times[-1] / 60}s")
			print(f"{counter} steps")

	showdataNP=np.array(showdata_s)
	showdataNP=cv.normalize(showdataNP,  showdata_s, 0, 255, cv.NORM_MINMAX)
	plt.imshow(showdataNP,cmap="gray",interpolation=None)
	img = Image.fromarray(showdataNP,'L')

	img.show()
	plt.show()

	# Use the plotting tools to show the eeg
	mne.viz.plot_raw(m.edfdata)
	plt.show()
	print("Done")