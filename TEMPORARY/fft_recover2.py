
import matplotlib.pyplot as plt
import mne
from scipy.fft import rfft, rfftfreq, irfft
from sklearn.preprocessing import normalize
import numpy as np
from scipy.integrate import simps
from eeghelpers import MNEDataset


file = "fft_attempts/trainingset17.edf"
data = mne.io.read_raw_edf(file)
raw_data = data.get_data()
# raw_data = np.delete(raw_data, 26, 0)
# raw_data = np.delete(raw_data, 23, 0)
# you can get the metadata included in the file and a list of all channels:
info = data.info
channels = data.ch_names
channel_count = info['nchan']
colour = np.empty(channel_count)
number = len(raw_data[0])

#channel selection
channel = 3

# storeeegft = np.zeros((channel_count, arrlenft))
# print(storeeegft.shape)
samplef = info['sfreq']
period = 1 / samplef

# # Label channels ... binary or multivariate outputs?
# channel_labels = []
# pattern1 = re.compile("[Ee][Kk][Gg]")
# pattern2 = re.compile("[Pp][Hh][Oo][Tt][Ii][Cc]")
# for n in range(channel_count):
#     # channellabels[n,0] = channels[n]
#     if pattern1.search(channels[n]) or pattern2.search(channels[n]):
#         print("%s is channel number %i" % (channels[n], n))
#         channel_labels = np.append(channel_labels, 0)
#     else:
#         channel_labels = np.append(channel_labels, 1)
#
# print(channel_labels)

m = MNEDataset(batch_step=.25, batch_window=.5)
data, times, last = m.get_next_batch()
freq = 1 / times[1]
length_sample = 2

###Calling functions directly
showdata_s, time_s = m.get_data_samples(0, m.samplerate*length_sample)  # this should be 1 seconds worth of samples = 2s given * 2
showdata_t, time2_t, last = m.get_data_time(0, 1 / 60)  # this is 1 second worth of samples should be same as above

# confirming they're the same before multiplying by 1.5 seconds (*1.5) for minimum HR 40
# ans = time_s - time2_t
# print(sum(ans))
#
# data = showdata_s - showdata_t
# print(sum(data))

fft_1 = rfft(showdata_s)
fft_freq = rfftfreq(len(showdata_s[channel]), period)
ifft_1 = irfft(fft_1)

fft_1_phase=np.angle(fft_1)
fft_1_phase_norm = normalize(fft_1_phase)
fft_1_mag=np.absolute(fft_1)
fft_1_mag_norm=normalize(fft_1_mag)

bintotal = 130
bins=np.linspace(0,freq/2,bintotal)
fft_1_bins=np.digitize(fft_freq,bins)

# I think this needs to be kept together (phase and mag) for the binning ... to ensure the total is accurate when the vectors are summed ... but I'm having a play

#separated magnitude and normalised magnitude with the phase
total_mag = []
total_mag_norm = []
total_phase = []
total_phase_norm = []

#kept together
fft_1_total = []

#running totals for fo loop
total = 0
total_p = 0
total_norm = 0
total_p_norm = 0
fft_1_bintotal = 0
bin = 0
count = 0
for n in fft_1_bins:
    if n == bin:
        total = total + fft_1_mag[channel, n]
        total_norm = total_norm + fft_1_mag_norm[channel, n]
        total_p = total_p + complex(np.cos(fft_1_phase[channel,n]), np.sin((fft_1_phase[channel,n])))
        total_p_norm = total_p_norm + complex(np.cos(fft_1_phase_norm[channel,n]), np.sin((fft_1_phase_norm[channel,n])))
        fft_1_bintotal = fft_1_bintotal + fft_1[channel,n]
        count += 1
    else:
        # wondering if it should be average bin value
        if count != 0:
            c = complex(count)
            total_mag.append(total/count)
            total_mag_norm.append(total_norm/count)
            total_phase.append(np.angle(total_p/count))
            total_phase_norm.append(np.angle(total_p_norm/count))
            fft_1_total.append(fft_1_bintotal/count)
        else:
            fft_1_total.append(fft_1_bintotal)
            total_mag.append(total)
            total_mag_norm.append(total_norm)
            total_phase.append(total_p)
            total_phase_norm.append(total_p_norm)
        # total_mag.append(total)
        # total_mag_norm.append(total_norm)
        # total_phase.append(total_p)
        # total_phase_norm.append(total_p_norm)
        #
        # fft_1_total.append(fft_1_bintotal)
        total = 0
        total_norm = 0
        total_p = 0
        total_p_norm = 0
        fft_1_bintotal = 0
        count = 0
        bin += 1

total_mag = np.array(total_mag)
total_mag_norm = np.array(total_mag_norm)
total_phase = np.array(total_phase)
total_phase_norm = np.array(total_phase_norm)
fft_1_total = np.array(fft_1_total)
# print(ifft_1.shape)
# print(showdata_s[0].shape)

#recover signal from amplitude/phase data in fft_1_total (already in real + imaginary)
recover = irfft(fft_1_total)/(length_sample/2)
duration = time_s[-1]
timebins = np.linspace(0,duration,len(recover))

#recover signal from separated data into amplitude and phase
#real = amplitude*cos(theta) im = amplitude*sin(theta)

recovered_1 = []
recovered_1_norm = []

n = 0
while n < bintotal:
    rec1 = complex(total_mag[n]*np.cos(total_phase[n]), total_mag[n]*np.sin(total_phase[n]))
    # rec_1_norm = complex(total_mag_norm[n]*np.cos(total_phase_norm[n]), total_mag_norm[n]*np.sin(total_phase_norm[n])) - this really doesn't work I think because you can't normalize the angles?
    rec_1_norm = complex(total_mag_norm[n] * np.cos(total_phase[n]), total_mag_norm[n] * np.sin(total_phase[n]))
    recovered_1.append(rec1)
    recovered_1_norm.append(rec_1_norm)
    n += 1

recovered_1 = np.array(recovered_1)
recovered_1_norm = np.array(recovered_1_norm)

# why = np.angle(fft_1_total) - total_phase
# print(why)

recover_1 = irfft(recovered_1)
recover_1_norm = irfft(recovered_1_norm)

plot = plt.figure(1)
plt.figure(1)
plt.figure(figsize = (20,12))
plt.subplot(1, 4, 1)
plt.plot(time_s, showdata_s[channel])
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.title('Raw Channel Data')

plt.subplot(1,4,2)
plt.plot(fft_freq, fft_1_mag[channel])
plt.xlabel('Frequency')
plt.ylabel('FFT Amplitude')
plt.title('FFT Data Amplitude (no bins)')

plt.subplot(1,4,3)
plt.plot(fft_freq, fft_1_phase[channel])
plt.xlabel('Frequency')
plt.ylabel('FFT Phase')
plt.title('FFT Data Phase (no bins)')

plt.subplot(1,4,4)
plt.plot(time_s, ifft_1[channel])
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.title('Recovered Channel Data (no bins)')

plot = plt.figure(2)
plt.figure(2)
plt.figure(figsize = (20,12))
plt.subplot(3,3,1)
plt.plot(bins, total_mag)
plt.xlabel('Frequency (100 Bins)')
plt.ylabel('Amplitude')
plt.title('Digitised samples')

plt.subplot(3,3,2)
plt.plot(bins, total_mag_norm)
plt.xlabel('Frequency (100 Bins)')
plt.ylabel('Amplitude')
plt.title('Digitised samples - normalized')

plt.subplot(3,3,3)
plt.plot(bins, abs(fft_1_total))
plt.xlabel('Frequency (100 Bins)')
plt.ylabel('Amplitude')
plt.title('Digitised samples - from fft_1')

plt.subplot(3,3,4)
plt.plot(bins, total_phase)
plt.xlabel('Frequency (100 Bins)')
plt.ylabel('Phase')
plt.title('Digitised samples')

plt.subplot(3,3,5)
plt.plot(bins, total_phase_norm)
plt.xlabel('Frequency (100 Bins)')
plt.ylabel('Phase')
plt.title('Digitised samples - normalized')

plt.subplot(3,3,6)
plt.plot(bins, np.angle(fft_1_total))
plt.xlabel('Frequency (100 Bins)')
plt.ylabel('Phase')
plt.title('Digitised samples - from fft_1')

plt.subplot(3,3,7)
plt.plot(timebins,recover_1/(samplef/bintotal))
plt.xlabel('Time (s) (100 bins)')
plt.ylabel('Amplitude')
plt.title('Recovered Channel Data (100 bins), separated')

plt.subplot(3,3,8)
plt.plot(timebins,recover_1_norm)
plt.xlabel('Time (s) (100 bins)')
plt.ylabel('Amplitude')
plt.title('Recovered Channel Data (100 bins), separated, normalised')

plt.subplot(3,3,9)
plt.plot(timebins,recover)
plt.xlabel('Time (s) (100 bins)')
plt.ylabel('Amplitude')
plt.title('Recovered Channel Data (100 bins)')

plot = plt.figure(3)
plt.figure(3)
plt.figure(figsize = (20,12))
plt.subplot(2, 4, 1)
plt.plot(time_s, showdata_s[channel])
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.title('Raw Channel Data')

plt.subplot(2,4,2)
plt.plot(fft_freq, fft_1_mag[channel])
plt.xlabel('Frequency')
plt.ylabel('FFT Amplitude')
plt.title('FFT Data Amplitude (no bins)')

plt.subplot(2,4,3)
plt.plot(fft_freq, fft_1_phase[channel])
plt.xlabel('Frequency')
plt.ylabel('FFT Phase')
plt.title('FFT Data Phase (no bins)')

plt.subplot(2,4,4)
plt.plot(time_s, ifft_1[channel])
plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.title('Recovered Channel Data (no bins)')

# plt.subplot(2, 4, 5)
# plt.plot(time_s, showdata_s[0])
# plt.xlabel('Time (s)')
# plt.ylabel('Amplitude')
# plt.title('Raw Channel Data')

plt.subplot(2,4,6)
plt.plot(bins, abs(fft_1_total))
plt.xlabel('Frequency (100 Bins)')
plt.ylabel('Amplitude')
plt.title('Digitised samples - from fft_1')

plt.subplot(2,4,7)
plt.plot(bins, np.angle(fft_1_total))
plt.xlabel('Frequency (100 Bins)')
plt.ylabel('Phase')
plt.title('Digitised samples - from fft_1')

plt.subplot(2,4,8)
plt.plot(timebins,recover/(samplef/bintotal))
plt.xlabel('Time (s) (100 bins)')
plt.ylabel('Amplitude')
plt.title('Recovered Channel Data (100 bins)')

plot = plt.figure(4)
plt.figure(4)
plt.figure(figsize = (20,12))
# plt.subplot(1,3,1)
plt.plot(time_s, showdata_s[channel], label = 'Raw')
plt.plot(timebins, recover/(samplef/bintotal), label = 'Recovered (together, corrected)')
plt.plot(timebins, recover_1, label = 'Recovered (separate, not corrected)') # + 3/samplef if you want to separate by 3 bins to demonstrate
# plt.plot(timebins, recover_1_norm/(samplef/bintotal), label = 'Recovered (separate, normalised (amplitude))')

plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.legend()
plt.title('Raw vs Recovered Signal')

plt.show()
figsize = (20,12)

fig, axs = plt.subplots(2, 1)
axs[0].plot(time_s, showdata_s[channel], label = 'Raw')
axs[0].plot(timebins, recover/(samplef/bintotal), label = 'Recovered (together, corrected)')
# axs[0].set_xlim(0, 2)
axs[0].set_xlabel('time')
axs[0].set_ylabel('Amplitude')
axs[0].grid(True)

cxy, f = axs[1].cohere(showdata_s[channel], recover/(samplef/bintotal), 100, 1. / period)
axs[1].set_ylabel('coherence')

fig.tight_layout()
plt.show()

#quantification via AUC (Simpson and trapezoidal)

aucsimp_raw = simps(showdata_s[channel], dx=period)
aucsimp_recover = simps(recover, dx=1/bintotal)
auctrapz_raw = np.trapz(showdata_s[channel], dx=period)
auctrapz_recover = np.trapz(recover, dx=1/bintotal)

print(f'\nError by comparative AUC trapezoidal is: {auctrapz_raw-auctrapz_recover}\n')
print(f"Error by comparative AUC Simpson's is: {aucsimp_raw-aucsimp_recover}\n")