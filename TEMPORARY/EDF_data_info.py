
import glob
import mne
import os
import re
import numpy as np

basefolder = "./EDFs"
edffiles = glob.glob(os.path.join(basefolder, "*.edf"))

ekglabelsdict = {}

EDF_info = {}
All_EDF_info = []
total_duration_s = 0

for n in np.arange(len(edffiles)):
    data = mne.io.read_raw_edf(edffiles[n])
    # raw_data = data.get_data()
    info = data.info
    channels = info.ch_names
    sfreq = info['sfreq']
    times = data.times
    freq = 1/(times[1]-times[0])
    for c in np.arange(len(channels)):
        if re.compile("EKG").search(channels[c]):
            ekglabelsdict["EKG"]=channels[c]
    duration = times[-1]-times[0]

    EDF_info["edf"] = edffiles[n]
    EDF_info["channels"] = len(channels)
    EDF_info["sfreq"] = sfreq
    EDF_info["freq"] = freq
    EDF_info["EKG"] = len(ekglabelsdict)
    EDF_info["duration"] = duration

    total_duration_s += duration

    All_EDF_info.append(EDF_info.copy())

total_duration_m = total_duration_s/60
for edfs in np.arange(len(All_EDF_info)):
    print(All_EDF_info[edfs])

#print(All_EDF_info)
print(total_duration_s)
print(total_duration_m)