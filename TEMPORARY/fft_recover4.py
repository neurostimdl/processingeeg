import matplotlib.pyplot as plt
import mne
from scipy.fft import rfft, rfftfreq, irfft, ifft
import numpy as np
import glob
import os
from walkthrough_eeghelpers import MNEDataset
import cmath

basefolder = "./fft_attempts"
edfs = glob.glob(os.path.join(basefolder, "*.edf"))
data = mne.io.read_raw_edf(edfs[1])

# file = "examples/EEG/00000417_s004_t000.edf"
# # file = "fft_attempts/00000017_.edf"
# data = mne.io.read_raw_edf(file)
raw_data = data.get_data()

info = data.info
channels = data.ch_names
channel_count = info['nchan']
colour = np.empty(channel_count)
number = len(raw_data[0])

m = MNEDataset(batch_step=.25, batch_window=.5)
data, times, last = m.get_next_batch()

#channel selection
channel = 23

samplef = info['sfreq']
period = 1 / samplef


freq = 1 / times[1]
length_sample = 2

###Calling functions directly
showdata_s, time_s = m.get_data_samples(0, m.samplerate*length_sample)  # this should be 1 seconds worth of samples = 2s given * 2
showdata_t, time2_t, last = m.get_data_time(0, 1 / 60)  # this is 1 second worth of samples should be same as above

fft_1 = rfft(showdata_s)
fft_freq = rfftfreq(len(showdata_s[channel]), period)
ifft_1 = irfft(fft_1)

# fft_1_c = fft_1[channel,:]

bintotal = 500
bins=np.linspace(0,freq/2,bintotal)
fft_1_bins=np.digitize(fft_freq,bins)
# fft_1_bins = np.array(fft_1_bins, dtype=int)


fft_1_total = np.zeros((channel_count, bintotal), dtype=complex)
for n in np.arange(bintotal):
    for c in np.arange(channel_count):
        for w in np.arange(len(fft_1_bins)):
            a = fft_1_bins[w] - 1
            fft_1_total[c, a] = fft_1[c, w]

# def anyzero(binlabel):
#     for n in np.arange(bintotal):
#         if all(binlabel) == 1:
#             return 1
#         else:
#             if binlabel[n] == 0:
#                 binlabel[n] = binlabel[n-1]
#             return (anyzero(binlabel))

# for n in np.arange(len(fft_1_total)):
#     if fft_1_total[n].real == 0 and fft_1_total[n].imag == 0:
#         fft_1_total[n] = fft_1_total[n - 1]

# print(fft_1_total)

#recover signal from amplitude/phase data in fft_1_total (already in real + imaginary)
recover = irfft(fft_1_total[channel])/(length_sample/2)
duration = time_s[-1]
timebins = np.linspace(0,duration,len(recover))

save = np.zeros((channel_count, bintotal), dtype=complex)
for n in np.arange(bintotal):
    for c in np.arange(channel_count):
        for w in np.arange(len(fft_1_bins)):
            if n == 0 or abs(fft_1_total[c,n]!= 0):
                save[c,n] = fft_1_total[c, n]
            else:
                save[c,n] = fft_1_total[c, n - 1]

take2 = irfft(save[channel, :])/(length_sample/2)
timebins2 = np.linspace(0, duration*bintotal/freq, len(take2))

take3 = m.cosreconstrction(y_time=time_s, fft_vals=save, fft_freqs=fft_freq, channel=channel)
timebins3 = np.linspace(0, duration*bintotal/freq, len(take3))

plot = plt.figure(4)
plt.figure(4)
plt.figure(figsize = (20,12))
# plt.subplot(1,3,1)
plt.plot(time_s, showdata_s[channel], label = 'Raw')
# plt.plot(timebins, recover, label = 'Recovered (together, corrected)')
plt.plot(timebins2, take2, label = 'Take2')
plt.plot(timebins3, take3, label = 'Take3')
# plt.plot(timebins, recover_1, label = 'Recovered (separate, not corrected)') # + 3/samplef if you want to separate by 3 bins to demonstrate
# plt.plot(timebins, recover_1_norm/(samplef/bintotal), label = 'Recovered (separate, normalised (amplitude))')

plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.legend()
plt.title('Raw vs Recovered Signal')

plt.show()