import numpy as np
import re
import os
from eeghelpers import DataManager, MNEDataset
import random


#define length of step/batch in seconds
window_sec = 3
step_sec = 1.5

m = MNEDataset(batch_step=step_sec/60, batch_window=window_sec/60)
#data, times, last = m.get_next_batch()

EEG_L = ["FP1", "F7", "F3", "A1", "T3", "C3", "T5", "P3", "O1"]
EEG_R = ["FP2", "F8", "F4", "A2", "T4", "C4", "T6", "P4", "O2"]
EEG_M = ["FZ", "CZ", "PZ"] # "NZ", "FPZ" , "OZ"

EEG_Labels = EEG_L + EEG_R + EEG_M

Xs=[]
Ys=[]
done=False

saveevery=5000
n_per_file=300
folder = os.path.abspath("EEG_TRAIN")
dm = DataManager(folder,testsplit=.2,batchsize=100,datapoolsize=10000)
i=0
filecount=0
while True:
    channels = m.channels
    info = m.info
    channel_count = info['nchan']
    samplerate = m.samplerate
    requiredl = int(window_sec*samplerate)

    eeglabelsdict = {}
    ekglabelsdict = {}
    for l in EEG_Labels:
        eeglabelsdict[l] = None

    for n in np.arange(len(channels)):
        for label in EEG_Labels:
            if re.compile(label).search(channels[n]):
                # label[n] = 1
                eeglabelsdict[label] = n
                break
            elif re.compile("EKG").search(channels[n]):
                ekglabelsdict["EKG"]=n
                break
    i=i+1
    thisEEG=random.choice(EEG_Labels)
    eegchan = eeglabelsdict[thisEEG]
    if eegchan == None:
        thisEEG = random.choice(EEG_Labels)
        eegchan = eeglabelsdict[thisEEG]
    #ekgchan = random.choice(list(ekglabelsdict.values()))
    ekgchan =ekglabelsdict["EKG"]
    allfinished=False
    try:
        data,times,done=m.get_next_batch(requiredl)
    except:
        allfinished=True

    if len(times)==requiredl:
        fft_val,freqs=m.get_fft(data)
        ekg_data = fft_val[ekgchan]
        eeg_data = fft_val[eegchan]
        ekg_mag = np.abs(ekg_data)
        ekg_angle= np.angle(ekg_data)
        eeg_mag = np.abs(eeg_data)
        eeg_angle = np.angle(eeg_data)
        eegX=list(zip(eeg_mag, eeg_angle))
        ekgX = list(zip(ekg_mag, ekg_angle))
        Xs.append(eegX)
        Ys.append([0])
        Xs.append(ekgX)
        Ys.append([1])
        if i % saveevery == 0:
            dm.saveData(X_true=Xs, Y_true=Ys, n_per_file=n_per_file)  #, folder="EKG Training"
            Xs = []
            Ys = []
    else:
        #fft_val2,freqs2=m.get_fft(data)
        #dm.saveData(X_true=Xs, Y_true=Ys, n_per_file=n_per_file)  #, folder="EKG Training"
        #Xs = []
        #Ys = []
        pass
    if done:
        filecount+=1
        print(m.edffiles[filecount-1])
        print(m.channels)
        if filecount>=len(m.edffiles):
            dm.saveData(X_true=Xs, Y_true=Ys, n_per_file=n_per_file)  # , folder="EKG Training"
            Xs = []
            Ys = []
            break
        else:
            done=False
    if allfinished:
        dm.saveData(X_true=Xs, Y_true=Ys, n_per_file=n_per_file)  # , folder="EKG Training"
        Xs = []
        Ys = []
        break

#dm.saveData(X_true=Xs, Y_true=Ys, folder="EKG Training", n_per_file=500)  #
# print(len(Xs))
# print(len(Ys))


