import os

from eeghelpers import *
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import precision_recall_curve

import sklearn
import numpy as np
#import mkl
#mkl.set_num_threads(50)
howmany=2000

dm = DataManager("EEG_TRAIN",testsplit=.2,batchsize=64,datapoolsize=5000)
print(f"Getting Test batch")
X_test,Y_test,finished=dm.get_testset_batch(howmany)
# X_test = X_test.reshape(X_test.shape[0], (X_test.shape[1] * X_test.shape[2]))

nepochs = 5000

classifier = MLPClassifier(hidden_layer_sizes=[60, 10, 5,5,5], activation = 'relu', solver = 'adam', max_iter = 1)
import pickle
breakthresh = 0.9999
fin=False
best=-1
X_test = X_test.reshape(X_test.shape[0], (X_test.shape[1] * X_test.shape[2]))
os.makedirs(f"network",exist_ok=True)
print(f"starting training")
for i in range(nepochs):
    done=False
    step=0
    while not done:
        X,Y,done=dm.get_batch()
        if not done: #note - just for convenience wont do the last batch because it might be smaller than batchsize.
            X = X.reshape(X.shape[0], (X.shape[1]*X.shape[2]))
            classcall = np.unique(Y.ravel())
            classifier.partial_fit(X,Y.ravel(), classes=classcall) #do one step
        score=classifier.score(X_test,Y_test)

        #Y_true = Y_test[:, 1]
        Y_pred_bin = classifier.predict_proba(X_test)
        #Y_predprobs = Y_pred_bin[:, 1]
        fpr1, tpr1, thresholds =  sklearn.metrics.roc_curve(Y_test.ravel(), Y_pred_bin[:,1].ravel(), pos_label=1)

        inv=np.logical_not(Y_test)
        fpr2, tpr2, thresholds2 =  sklearn.metrics.roc_curve(inv.ravel(), Y_pred_bin[:, 0].ravel(), pos_label=1)

        roc_auc1 = sklearn.metrics.auc(fpr1, tpr1)
        roc_auc2 = sklearn.metrics.auc(fpr2, tpr2)

        if roc_auc1 > best:
            best = roc_auc1
            isbest = True
            pickle.dump(classifier, open(f"network/classifier{i}_{step}.mlp", 'wb'))

        else:
            isbest = False

        print(f"\r step: {dm.batchcounter} \t score:{score} \t test_rocECG:{roc_auc1} \t test_rocEEG:{roc_auc2} {['','*'][isbest]} ",end="")

        if roc_auc1>breakthresh:
            print(f"\n Finished")
            fin=True
            break
        step+=1

    print(f"\r------ epoch: {dm.epochcounter} \t {[' ','*'][isbest]}score:{score} ---------------------")
    if fin==True:
        break

Y_true = Y_test[:, 1]
Y_pred_bin = classifier.predict_proba(X_test)
Y_predprobs = Y_pred_bin[:, 1]
fpr1, tpr1, thresholds =  sklearn.metrics.roc_curve(Y_true, Y_predprobs, pos_label=1)
tn, fp, fn, tp =sklearn.metrics.confusion_matrix(Y_true,Y_predprobs).ravel()

#load with
# loaded_model = pickle.load(open(filename, 'rb'))
n=5
actual = Y_test[:n]
print(f"PredictBin:{Y_pred_bin[:n]} \n Actual:{actual}")
print(f"----------------------------------------------------------------")
print(f"PredictProbs:{Y_predprobs[:n]} \n Actual:{actual}")

#confusion=sklearn.metrics.confusion_matrix(Y_test[:1000],Y_pred[:1000])
#print(confusion)