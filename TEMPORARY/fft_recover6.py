import matplotlib.pyplot as plt
import mne
from scipy.fft import rfft, rfftfreq, irfft, ifft
import numpy as np
from walkthrough_eeghelpers import MNEDataset

m = MNEDataset(batch_step=.25, batch_window=.5)
data, times, last = m.get_next_batch()
channels = m.channels
info = m.info
channel_count = info['nchan']
number = len(data[0])

# choose channel for analysis
channel = 23

samplef = info['sfreq']
period = 1 / samplef

freq = 1 / times[1]
length_sample = 2

fft_1 = rfft(data)
fft_t2, freqs_2 = m.get_fft(data)

fft_freq = rfftfreq(len(data[channel]), period)



ifft_1 = irfft(fft_1)
ifft_t1 = m.get_ifft(fft_1)
ifft_t1_1 = m.get_ifft(fft_t2)

ifft_t2 = m.cosreconstrction(times,fft_1, fft_freq, channel)
ifft_t2_1 = m.cosreconstrction(times, fft_t2, fft_freq, channel)

time_recon = np.linspace(0, times[-1], len(ifft_t1[channel]))

plot = plt.figure(1)
plt.figure(1)
plt.figure(figsize = (20,12))

plt.subplot(3,2,1)
plt.plot(fft_freq, fft_1[channel], label = 'fft_1')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Amplitude of signal')
plt.title('Frequency analysis - fft_1')
plt.subplot(3,2,2)
plt.plot(freqs_2, fft_t2[channel], label = 'fft_t2')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Amplitude of signal')
plt.title('Frequency analysis - fft_2')

plt.subplot(3,2,3)
plt.plot(fft_freq, np.abs(fft_1[channel]), label = 'fft_1')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Magnitude of signal')
plt.subplot(3,2,4)
plt.plot(freqs_2, np.abs(fft_t2[channel]), label = 'fft_t2')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Magnitude of signal')

plt.subplot(3,2,5)
plt.plot(fft_freq, np.angle(fft_1[channel]), label = 'fft_1')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Phase of signal')
plt.subplot(3,2,6)
plt.plot(freqs_2, np.angle(fft_t2[channel]), label = 'fft_t2')
plt.xlabel('Frequency (Hz)')
plt.ylabel('Phase of signal')

plt.legend()


plot = plt.figure(2)
plt.figure(2)
plt.figure(figsize = (20,12))
plt.plot(times, data[channel], label = 'Raw')
plt.plot(times, ifft_1[channel], label = 'ifft_1')
plt.plot(time_recon, ifft_t1[channel], label = 'ifft_t1')
plt.plot(time_recon, ifft_t1_1[channel], label = 'ifft_t1_1')
plt.plot(times, ifft_t2, label = 'ifft_t2')
plt.plot(times, ifft_t2_1, label = 'ifft_t2_1')

# plt.plot(timebins, recover_1, label = 'Recovered (separate, not corrected)') # + 3/samplef if you want to separate by 3 bins to demonstrate
# plt.plot(timebins, recover_1_norm/(samplef/bintotal), label = 'Recovered (separate, normalised (amplitude))')

plt.xlabel('Time (s)')
plt.ylabel('Amplitude')
plt.legend()
plt.title('Raw vs Recovered Signal')



bintotal = 500
bins=np.linspace(0,freq/2,bintotal)
fft_bins=np.digitize(fft_freq,bins)

fft_1_total = np.zeros((channel_count, bintotal), dtype=complex)
for n in np.arange(bintotal):
    for c in np.arange(channel_count):
        for w in np.arange(len(bins)):
            a = fft_bins[w] - 1
            fft_1_total[c, a] = fft_1[c, w]

ifft_b1 = irfft(fft_1_total)
# ifft_b2 = ifft_t2 = m.cosreconstrction(times, fft_1_total, fft_freq, channel)

plot = plt.figure(3)
plt.figure(3)
plt.figure(figsize = (20,12))

plot_bin_t = np.linspace(0,times[-1],len(ifft_b1[channel]))

plt.plot(fft_freq, fft_1[channel])
plt.plot(bins, fft_1_total[channel], label='irfft')
# plt.plot(fft_freq, ifft_b2, label='cosrecon')

plot = plt.figure(4)
plt.figure(4)
plt.figure(figsize = (20,12))

plt.plot(times, data[channel], label = 'Raw')
plt.plot(plot_bin_t, ifft_b1[channel])

plt.show()