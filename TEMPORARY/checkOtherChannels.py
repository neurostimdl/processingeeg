import random

import matplotlib.pyplot as plt
import sklearn
import pickle
from eeghelpers import *
if __name__ == '__main__':
	i=2
	model= pickle.load(open(f"network/classifier3_30.mlp", 'rb'))
	#window_sec = 3
	#step_sec = 1.5
	window_sec = 3
	step_sec = 1.5

	EEG_L = ["FP1", "F7", "F3", "A1", "T3", "C3", "T5", "P3", "O1"]
	EEG_R = ["FP2", "F8", "F4", "A2", "T4", "C4", "T6", "P4", "O2"]
	EEG_M = ["FZ", "CZ", "PZ"]
	EEG_Labels = EEG_L + EEG_R + EEG_M
	ECG_Labels=["ECG","EKG"]
	ALL_Labels=EEG_Labels+ECG_Labels
	m = MNEDataset(batch_step=step_sec / 60, batch_window=window_sec / 60)
	#alldata=m.get_next_batch()

	###get other reference channels
	REF_LABELS=[]
	EEG_chan=[]
	ECG_chan=[]
	REF_chan=[]
	labels={}
	labels["ref"]=[]
	channeldict={} #reference by channel name
	for idx,chan in enumerate(m.channels):
		found=False
		channeldict[chan] = idx
		for label in ECG_Labels:
			if chan.find(label)!=-1:
				ECG_chan.append(idx)
				found=True
				if label in labels.keys():
					newlist=[].extend(labels[label])
					newlist.append(idx)
					labels[label]=newlist

				else:
					labels[label]=idx

				###not breaking in case a channel is dual labelled
		for label in EEG_Labels:
			if chan.find(label)!=-1:
				EEG_chan.append(idx)
				labels[label] = idx
				found=True
		if not found:
			if chan in labels.keys():
				labels["ref"].append(idx)
			else:
				labels["ref"]=[idx]

			REF_chan.append(idx)

	fin=False
	Y_true = np.zeros(shape=(len(m.channels)))
	Y_true[EEG_chan]=0
	Y_true[ECG_chan] = 1
	#Y_true[REF_chan] = [0, 0]
	eegresult=np.zeros(shape=(len(m.channels)))
	ecggresult=np.zeros(shape=(len(m.channels)))
	counter=0
	while not fin:
		data,times,fin = m.get_next_batch()

		if fin: break
		fft_val, freqs = m.get_fft(data)
		fixednbins=len(freqs)-1##you need to fix the n bins in case sample rate changes each edf
		mags = np.abs(fft_val)
		angles = np.angle(fft_val)
		alldata = np.array(list(zip(mags, angles)))
		alldata = alldata.reshape(alldata.shape[0], (alldata.shape[1] * alldata.shape[2]))
		Y_pred = model.predict(alldata)
		tru=Y_true
		chan=random.choice(EEG_chan)
		score=model.score(alldata[:24],tru[:24])
		#if score<60:
		fig, axs = plt.subplots(2, 1)
		axs[0].plot(times,data[0])
		axs[1].plot(times,data[23])

		if Y_pred[0]==0:
			axs[0].text(max(times),min(data[0]),"correct")
		else:
			axs[0].text(max(times),min(data[0]),"INCORRECT")

		if Y_pred[23]==1:
			axs[1].text(max(times),min(data[23]),"correct")
		else:
			axs[1].text(max(times),min(data[23]),"INCORRECT")

		plt.show()
		#correct=np.all(Y_pred==Y_true,axis=1) # and Y_pred[1]==Y_true[1])
		#ncorrect=np.sum(correct)
		#eegresult+=Y_pred[:,0]
		ecggresult += Y_pred

		#results+=Y_pred
		counter+=1
	for idx,chan in enumerate(m.channels):



		pass

	#for ref in REF_chan: #now cycle through all the reference channels and lets see how the prediction goes

