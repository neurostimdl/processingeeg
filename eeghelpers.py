import mne  #pip install mne
import os
import numpy as np
import glob
import matplotlib.pyplot as plt
from PIL import Image
import cv2 as cv
from scipy.fft import rfft, rfftfreq, irfft, ifft
import cmath
import json_tricks    #pip install json-tricks  #this handles numpy and additional datatypes which standard json does not.
from utils import *
import glob
import os
import random
from abc import ABC, abstractmethod #abstractclass

class EEGDatasetBase(ABC):  #this is the baseclass to inherit from

	@abstractmethod
	def get_sample_rate(self) -> int:
		assert False #overwrite this. Because figuring out sample rate might differ between file formats

	@abstractmethod
	def init_file_reading(self):
		##This is customised .ext dependant. Prepare the data stream for access
		# It could read all the data from the file or establish the required variables for getbatch to stream data.
		assert False ##You must override this function below is an example for loading edf
		self.edfdata = mne.io.read_raw_edf(self.dataFiles[self.file_idx])
		# these are the things that might be different with each EDF.
		# call everytime the edf changes
		self.last_sample = self.edfdata.last_samp
		self.info = self.edfdata.info  # https://mne.tools/stable/generated/mne.io.read_raw_edf.html
		self.channels = self.edfdata.ch_names


		raw_data, times = self.edfdata.get_data(start=0, stop=10, return_times=True)  # get 10 samples
		est_samplerate = 1 / ((times[-1] - -times[0]) / 9)
		est_samplerate2 = 1 / (times[1] - times[0])
		if samplerate != None:
			self.samplerate = int(samplerate)
		else:
			try:
				self.samplerate = int(self.info['sfreq'])
			except:
				self.samplerate = est_samplerate2
		if est_samplerate2 != est_samplerate or est_samplerate != self.samplerate:
			if samplerate != None:
				self.samplerate = int(samplerate)
			else:
				self.samplerate = int(est_samplerate2)
			print(f"SAMPLE RATE CONFLICT IN {self.current_file()} line 50 eeghelpers.py. {self.samplerate}Hz set.")
		# info = mne.create_info(ch_names=channels, sfreq=sr,ch_types="eeg").set_montage(montage)
		# https://www.researchgate.net/profile/Heidelise_Als/publication/228064755/figure/fig1/AS:203120069091344@1425439006780/Standard-EEG-electrode-names-and-positions-Head-in-vertex-view-nose-above-left-ear-to.png
		# data.plot()
		self.layout = mne.channels.read_layout('biosemi')
		self.rawtimes = self.edfdata.times  ##all the timestepsp in this file

	@abstractmethod
	def get_data_samples(self,start,stop,return_times=True):
		assert False #override this function
		raw_data,times = self.edfdata.get_data(start=start,stop=finish,return_times=return_times)
		return raw_data,times

	#@abstractmethod
	def get_data_time(self, start_time_s, duration_s):
		sample_S = int(start_time_s * self.samplerate)
		sample_finish = int(np.ceil((start_time_s + duration_s) * self.samplerate))
		last = False

		raw_data, times,last = self.get_data_samples(start=sample_S, stop=sample_finish, return_times=True)

		return raw_data, times, last

	def __init__(self, batch_step_s=.5, batch_window_s=1, samplerate = 250, lastFileAction=0, lastSampleAction=0,folder="./test",ext=".edf"):
		"""
		:param batch_step:
		:param batch_window:
		:param samplerate:
		:param lastFileAction: 0: quit. 1. looparound.
		:param lastSampleAction: 0: loadNextEdf. 1. looparound.
		"""
		self.basefolder = folder
		self.ext=ext
		self.lastFileAction=lastFileAction
		self.lastSampleAction=lastSampleAction
		self.reset()
		self.inc_file() #increments which file is being processed

		self.last_sample=self.init_file_reading() #gets the data for the existing fileidx
				##Make sure self.last_sample is returned from  self.init_file_reading(). it is the index for the last sample.

		if samplerate != None:
			self.samplerate = int(samplerate)
		else:
			self.samplerate=int(self.get_sample_rate())
		self.batch_window_samples= int(self.samplerate * batch_window_s) #how wide the window is in samples
		self.batch_step_samples=int(self.samplerate*batch_step_s)
		self.batch_step_s = batch_step_s  # this how much we increment step in time.
		self.batch_window_s = batch_window_s  # this is how wide the window is in time.

	def reset(self):
		#resets back to the first file.
		self.dataFiles = glob.glob(os.path.join(self.basefolder, f"*{self.ext}"))
		self.file_idx = -1
		self.idx = 0  # this is how many time steps have been completed.

	def inc_file(self):
		self.file_idx+=1
		if self.file_idx>=len(self.dataFiles):
			print(f"FINISHED  last file ")
			if self.lastFileAction==0:
				exit(0)
			elif self.lastFileAction==1:
				self.reset()
				print("LOOPING AROUND")

	def get_current_file(self):
		return self.dataFiles[self.file_idx], self.file_idx

	def get_next_batch(self):
		#gets the next time step of data from the EEG.
		#start_time=self.idx*self.batch_step_s
		start_sample=self.idx*self.batch_step_samples
		fin_sample=start_sample+self.batch_window_samples

		#data1,times1,last1=self.get_data_time(start_time,self.batch_window_s)
		data, times, last = self.get_data_samples(start_sample, fin_sample)

		if last:
			if self.lastSampleAction==0:
				self.inc_file()
			elif self.lastSampleAction==1:
				self.idx=0

		else:	self.idx=self.idx+1

		return data,times,last

	def get_fft(self,data,nbins=None):
		#period=1/self.samplerate
		if nbins==None:
			fft_vals = rfft(data)
			fft_freqs = rfftfreq(len(data[0]), 1/self.samplerate) #freqs are tge same for each channel
		else:
			fft_vals = rfft(data,n=nbins*2)
			fft_freqs = rfftfreq(nbins*2, 1/self.samplerate) #freqs are tge same for each channel
		if len(fft_vals.shape)==1: #in case  this is just a single fft make sure it has consistant shape with multichannel fft.
			np.expand_dims(fft_vals,0)
		return fft_vals,fft_freqs

	def get_ifft(self,data,n=None):
		"""
		:param data: The FFT data
		:param n: number of bins
		:return: the ifft with the given number of bins
		"""
		if n==None: n=len(data[0])*2+1
		ifft = irfft(data,n)

		return ifft

	def cosreconstrction(self, y_time, fft_vals, fft_freqs, channel,proptozero=.8):
		##This reconstructs the signal from the frequency and phase components INSTEAD OF IFFT.
		##z=r(cos(phi)+isin(phi)
		#cos is the real component, r is mag and phi is the angle
		##why do this?? when you do an ifft for a reduced number of bins it will produce a smaller time period gving the impression that the frequency binning is not working properly.
		#in actual fact most of the information still exists in the frequency domain with less bins - despite the IFFT function failing to reproduce the exact function - by reconstructing with cos we can get a reconstruction for any time length.

		vals=fft_vals[channel]
		mag = np.absolute(vals)/len(fft_freqs)
		ts=y_time
		#zeros=np.where(mag==0)
		phase = np.angle(vals)
		#phase[zeros]=0.0
		ws = 2. * np.pi * fft_freqs

		#mag[mag<1]=0.0
		k=int(proptozero*len(mag)) #zero out the bottom 20% of values
		idx = np.argpartition(mag, k)
		smallestidx=idx[:k]
		mag[smallestidx]=0

		fn=np.zeros_like(ts)
		for i in range(len(fft_freqs)):
			f = fft_freqs[i]
			if mag[i]!=0:
				#print(mag[i])
				a=9.96*np.sin(62.83*ts)
				z=mag[i]*np.complex(np.cos(phase[i]),np.sin(phase[i]))

			#ws=2. * np.pi * f
			fn=fn+mag[i] * np.cos(ws[i] * ts +phase[i])

			#fn=fn+mag[i] * (np.sin(ws[i] * ts)



		return fn

	def movingaverage(self,interval, window_size):
		window = np.ones(int(window_size)) / float(window_size)

		val= np.convolve(interval, window, 'same')
		return val

	def get_freq_bands(self,fft,freqs):
		"""
		This returns the psd in each of the common frequency bands. delta (1–3 Hz), theta (4–7 Hz), alpha (8–12 Hz), beta (13–30 Hz),  gamma (30–100 Hz) HighF >100Hz.
		:param fft:
		:return:
		"""
		fbands=[3,7,12,30,100,10000]
		fband_name=["delta","theta","alpha","beta","gamma","hf"]
		bands=[]
		fbandcounter=0
		counter=0  #counts how many bins so we can get the average power
		bands=np.zeros(shape=(fft.shape[0],len(fband_name)))

		for idx,val in enumerate(freqs):
			if val>fbands[fbandcounter]:
				###then next index

				##bands[fband_name[fbandcounter]]=bands[fband_name[fbandcounter]]/counter  ##IF YOU WANT THE AVERAGE POWER FOR THE BINS

				fbandcounter+=1
				splice=fft[:,idx]
				conj=np.conjugate(splice)
				psd=np.absolute(splice * conj)
				bands[:,fbandcounter] = psd
				counter=1
			else:
				##this one already exists
				#bands[fband_name[counter]]+= b

				#bands[fband_name[counter]]=np.add(bands[fband_name[counter]],b)
				splice = fft[:, idx]
				conj = np.conjugate(splice)
				psd=np.absolute(splice * conj)
				bands[:,fbandcounter] += psd
				counter += 1

				#bands[fband_name[counter]] += fft[:][idx]
		return bands,fband_name

class MNEDataset(EEGDatasetBase):
	def __init__(self, batch_step_s=.5, batch_window_s=1, samplerate = None, lastFileAction=0, lastSampleAction=0, folder=None,ext=".edf",checksampleRate=False):
		"""
		:param batch_step:
		:param batch_window:
		:param samplerate:
		:param lastFileAction: 0: quit. 1. looparound.
		:param lastSampleAction: 0: loadNextEdf. 1. looparound.
		"""
		if folder==None:
			self.basefolder="./EEGData"
		else:
			self.basefolder=folder

		super().__init__(batch_step_s=batch_step_s, batch_window_s=batch_window_s, samplerate = samplerate,
						 lastFileAction=lastFileAction,lastSampleAction=lastSampleAction,folder=self.basefolder,ext=ext)
		##now do edf specific stuff
		#self.last_sample=self.edfdata.last_samp
		self.info = self.edfdata.info  # https://mne.tools/stable/generated/mne.io.read_raw_edf.html
		self.channels = self.edfdata.ch_names
		#just check the samples to make sure its all good.
		if checksampleRate:
			raw_data, times = self.edfdata.get_data(start=0, stop=10, return_times=True) #get 10 samples

			est_samplerate=1/((times[-1]--times[0])/9)
			est_samplerate2=1/(times[1]-times[0])
			if samplerate!=None:
				self.samplerate=int(samplerate)
			else:
				try:
					self.samplerate =  int(self.get_sample_rate())
				except:
					self.samplerate=int(est_samplerate2)
					print(f"Sample rate not in info. Had to get an estimate of sample rate.")

		self.layout = mne.channels.read_layout('biosemi') #
		# info = mne.create_info(ch_names=channels, sfreq=sr,ch_types="eeg").set_montage(montage)
		# https://www.researchgate.net/profile/Heidelise_Als/publication/228064755/figure/fig1/AS:203120069091344@1425439006780/Standard-EEG-electrode-names-and-positions-Head-in-vertex-view-nose-above-left-ear-to.png
		# data.plot()

	def get_sample_rate(self):
		samplerate = self.info['sfreq']
		return samplerate

	def init_file_reading(self):
		self.edfdata = mne.io.read_raw_edf(self.dataFiles[self.file_idx])
		self.info=self.edfdata.info
		self.channels=self.edfdata.ch_names
		EEG_L = ["A1", "T3", "C3", "T5", "P3", "O1"]
		EEG_L = EEG_L+["FP1","AF7", "F9", "F7", "F5", "F3", "F1"]
		EEG_L = EEG_L + ["FT9","FT7","FC5","FC3","FC1"]
		EEG_L = EEG_L + ["FT9","FT7","FC5","FC3","FC1"]

		EEG_R = ["FP2", "F8", "F4", "A2", "T4", "C4", "T6", "P4", "O2"]
		EEG_M = ["NZ", "FPZ", "FZ", "CZ", "PZ", "OZ"]
		self.eeglabelsdict = {}
		self.eegchannels = []  # list of all the eeg channels
		self.EEG_Labels = EEG_L + EEG_R + EEG_M
		for l in self.EEG_Labels:
			self.eeglabelsdict[l] = None
		channels = self.channels

		for label in self.EEG_Labels:  # only the eeg channels.
			self.eeglabelsdict[label] = None
			found=False
			for n in np.arange(len(channels)):

				if re.compile(label).search(channels[n]):
					# label[n] = 1
					self.eeglabelsdict[label] = n
					self.eegchannels.append(n)
					found=True
					break
			if not found:
				print(f"LABEL NOT FOUND: {label}")

		return self.edfdata.last_samp

	def get_data_samples(self,start:int,stop:int,return_times=True):

		if stop >= self.last_sample:
			last = True  #last can be discovered by uing try:except
		else:
			last=False

		raw_data,times = self.edfdata.get_data(start=start,stop=stop,return_times=return_times)
		return raw_data,times,last

class HDF5Dataset(EEGDatasetBase):
	def __init__(self, batch_step=.5, batch_window=1, samplerate = 250, lastFileAction=0, lastSampleAction=0,folder="test",ext=".hdf5"):
		"""  STILL A WORK IN PROGRESS
		:param batch_step:
		:param batch_window:
		:param samplerate:
		:param lastFileAction: 0: quit. 1. looparound.
		:param lastSampleAction: 0: loadNextEdf. 1. looparound.
		"""
		super().__init__(batch_step=batch_step, batch_window=batch_window, samplerate = samplerate,
						 lastFileAction=lastFileAction,lastSampleAction=lastSampleAction,folder=folder,ext=ext)

	def get_data_time(self,start_time_mins,duration_mins):
		sample_S=int(60*start_time_mins*self.samplerate)
		sample_finish=int(60*(start_time_mins+duration_mins)*self.samplerate)
		last=False
		if sample_finish>self.last_sample:
			raw_data,times = self.edfdata.get_data(start=sample_S,stop=self.last_sample,return_times=True)
			last = True
		else:
			#raw_data,times = self.edfdata.get_data_samples(start=sample_S,stop=sample_finish,return_times=True)
			raw_data,times = self.edfdata.get_data(start=sample_S,stop=sample_finish,return_times=True)

		return raw_data,times,last

	def get_data_samples(self,start,stop,return_times=True):
		raw_data,times = self.edfdata.get_data(start=start,stop=stop,return_times=return_times)
		return raw_data,times
	def plot_recon(self, orig_sig, times,channel=0, nbins=125,mvavpts=10):

		fft_vals, fft_freqs = self.get_fft(orig_sig,nbins=nbins) #
		recon=self.cosreconstrction(times, fft_vals, fft_freqs, channel=channel)
		#plt.ioff()
		plt.figure(1)
		orig_chan=orig_sig[channel]
		plt.subplot(3, 1, 1)
		plt.plot(times, orig_chan)
		plt.plot(times[:len(recon)], recon[:len(times)])
		plt.xlabel('Time (s)')
		plt.ylabel('Amp')
		plt.title(f'EEG CH{channel}')
		######################################################
		orig_fft_vals, orig_fft_freqs = self.get_fft(orig_sig) #
		recon_fft_vals, recon_fft_freqs = self.get_fft([recon],nbins=nbins) #
		plt.subplot(3, 1, 2)
		origabs=np.absolute(orig_fft_vals[channel])
		origabs=origabs/np.max(origabs)
		(markerline, stemlines, baseline)=plt.stem(orig_fft_freqs, origabs,use_line_collection=True, markerfmt='.',label=f"Orig {len(origabs)} bins")
		plt.setp(baseline, visible=False)
		plt.setp(stemlines, 'color', plt.getp(markerline, 'color'))
		plt.setp(stemlines, 'linestyle', 'dashdot')
		plt.setp(markerline, 'markersize', '7')
		reconabs=np.absolute(recon_fft_vals[0])
		reconabs=reconabs/np.max(reconabs)
		(markerline, stemlines, baseline)=plt.stem(recon_fft_freqs, reconabs,use_line_collection=True, markerfmt='.',label=f"Recon {len(reconabs)} bins")
		plt.setp(baseline, visible=False)
		plt.setp(stemlines, 'color', plt.getp(markerline, 'color'))
		plt.setp(stemlines, 'linestyle', 'dotted')
		plt.setp(markerline, 'markersize', '6')
		plt.xlabel('Freq (Hz)')
		plt.ylabel('Mag')
		plt.legend(loc="best", prop={'size': 6})

		######################################################
		ax=plt.subplot(3, 1, 3)
		delta=recon[:len(orig_chan)]-orig_chan[:len(recon)]
		mvav=self.movingaverage(delta,mvavpts)
		(markerline, stemlines, baseline) = plt.stem(times[:len(delta)], delta[:len(times)],use_line_collection=True, markerfmt='.',label="Diff")
		plt.plot(times,mvav,label=f"MAv {mvavpts}p")
		plt.setp(baseline, visible=False)
		plt.setp(stemlines, 'color', plt.getp(markerline, 'color'))
		plt.setp(stemlines, 'linestyle', 'dotted')
		plt.setp(markerline, 'markersize', '6')
		plt.xlabel('Time (s)')
		plt.ylabel('diff Amp')
		plt.legend(loc="best", prop={'size': 6})
		av=np.average(delta)
		sum=np.sum(delta)

		print(f"Time Error Sum:{sum}, Av:{av}")
		ax.text(0,0,f"ERR\n Sum:{np.round(sum,1)}\n Av:{np.round(av,1)}", fontsize=7,bbox={'facecolor': 'red', 'alpha': 0.05, 'pad': 2})
		plt.tight_layout()

		#plt.show()
		######################################################
		plt.figure(2)
		plt.subplot(3, 1, 1)
		plt.plot(times, orig_chan)
		plt.plot(times[:len(recon)], recon[:len(times)])

		plt.xlabel('Time (s)')
		plt.ylabel('Amp')
		plt.title(f'EEG CH{channel}')
		######################################################
		plt.subplot(3, 1, 2)
		#plt.plot(fft_freqs, np.angle(fft_vals[channel]))
		#plt.plot(recon_fft_freqs, np.angle(recon_fft_vals[channel]))
		(markerline, stemlines, baseline)=plt.stem(fft_freqs, np.angle(fft_vals[channel]),use_line_collection=True, markerfmt='.')
		plt.setp(baseline, visible=False)
		plt.setp(stemlines, 'color', plt.getp(markerline, 'color'))
		plt.setp(stemlines, 'linestyle', 'dotted')
		plt.setp(markerline, 'markersize', '7')
		(markerline, stemlines, baseline)=plt.stem(recon_fft_freqs, np.angle(recon_fft_vals[0]),use_line_collection=True, markerfmt='.')
		plt.setp(baseline, visible=False)
		plt.setp(stemlines, 'color', plt.getp(markerline, 'color'))
		plt.setp(stemlines, 'linestyle', 'dotted')
		plt.setp(markerline, 'markersize', '6')

		plt.xlabel('Freq (Hz)')
		plt.ylabel('Phase')
		######################################################
		plt.subplot(3, 1, 3)
		delta=np.angle(recon_fft_vals[0])[:len(fft_vals[channel])]-np.angle(fft_vals[channel])[:len(recon_fft_vals[0])]
		#plt.plot(recon_fft_freqs[:len(delta)], delta[:len(recon_fft_freqs)])
		mvav=self.movingaverage(delta,mvavpts)
		(markerline, stemlines, baseline) = plt.stem(recon_fft_freqs[:len(delta)], delta[:len(recon_fft_freqs)],
													 use_line_collection=True, markerfmt='.',label="Diff")
		plt.plot(recon_fft_freqs, mvav, label=f"MAv {mvavpts}p")
		plt.setp(baseline, visible=False)
		plt.setp(stemlines, 'color', plt.getp(markerline, 'color'))
		plt.setp(stemlines, 'linestyle', 'dotted')
		plt.setp(markerline, 'markersize', '6')
		plt.legend(loc="best", prop={'size': 6})
		plt.xlabel('Freq (s)')
		plt.ylabel('diff Phase')
		av=np.average(delta)
		sum=np.sum(delta)
		print(f"Phase Error Sum:{sum}, Av:{av}")
		plt.text(0,0,f"ERR\n Sum:{np.round(sum,1)}\n Av:{np.round(av,1)}", fontsize=7,bbox={'facecolor': 'red', 'alpha': 0.05, 'pad': 2})
		plt.tight_layout()
		plt.show()

		##get mag and phase for the specific channel
		#plt.figure(2)
		freqs=[]
		mags=[]
		phases=[]
		# for key,val in mphasedic.items():
		# 	if key==0: continue #skip the dc component
		# 	freqs.append(key)
		# 	mag,phase=val[channel]
		# 	mags.append(mag)
		# 	phases.append(phase)
		# plt.subplot(4, 1, 2)
		# plt.plot(freqs, mags)
		# plt.xlabel('Freq (Hz)')
		# plt.ylabel('Magnitude')
		# plt.subplot(4, 1, 3)
		# plt.plot(freqs, phases)
		# plt.xlabel('Freq (Hz)')
		# plt.ylabel('Phase')
		# #plt.title(f'EEG CH{channel}')

		plt.show()
		print("finished")

class DataManager():
	def __init__(self,folder,testsplit=.2,batchsize=600,datapoolsize=1200):
		"""
		helper functions to manage data for ANN training which is too big to fit into memory.
		:param folder: the folder where the data is to be stored
		:param testsplit: the proportion of files to be held back from the dataset for testing
		:param batchsize: the size of each batch
		:param datapoolsize: the size of the datapool with which to create individual files. To minimise the chance of correlated data being stored in each file.
		"""
		#testsplit is the proportion of data to hold back as the test set.
		#note that it does not hold back exactly the data instead it simply holds back that proportion of files.
		#There could be issues with correlation of data if your files are highly correlated.
		#
		self.testsplit=0.2
		self.folder=folder
		os.makedirs(folder, exist_ok=True)
		self.filenames = glob.glob(f"{folder}/*.json")  # get current file names.
		n=int(testsplit*len(self.filenames))
		idxes=np.arange(0,len(self.filenames))
		testidxes=np.random.choice(idxes,n,replace=False)
		self.testfiles=list(np.array(self.filenames)[testidxes])
		self.filenames=list(np.delete(self.filenames, testidxes))

		self.filenamesremaining=[]  #as files are loaded they are removed from this list so we dont load them twice in one epoch

		self.batchsize=batchsize
		self.datapoolsize=datapoolsize #this is a multiple of how many more data points to load to reduce correlation of data
		self.epochstarted=False #is True after the first batch is done.
		self.storedX=[]
		self.storedY=[]
		self.lastfile=False
		self.batchcounter=0  #a counter that keeps track of how many batches have been done
		self.epochcounter=0
		self.testset_started=False
	def saveData(self,X_true,Y_true,n_per_file=10,randomise=True,folder=None):
		"""
		Saves ground truth data conveniently.
		:param X_true,Y_true: X_true is an n-D numpy array/tensor. Y-true is an n vector representing the ground truth value for X-true
		:param filename: The folder to save the data
		:param n_per_file=10: how many samples to save per file. Using 1 means that each example will be in its own file.
				A smaller number is efficient when randomising data to train but will be slower to load.
				Larger numbers will be more efficient to load, but may mean that randomisations are not truly random cause the same n datapoints may always occur together.
				If all the data fits into memory at once then you can overcome these points. If however you need to load data then the smaller the n the better.
		:param randomise=True: this randomises the datapool before selecting data to be stored in a file - it will decrease the chance of data being correlated within the same file.
		:return:
		"""
		if folder==None:
			self.filenames=glob.glob(f"{self.folder}/*.json") 		#get current file names.
		else:
			self.filenames = glob.glob(f"{folder}/*.json")  # get current file names.

		data=list(zip(X_true,Y_true))
		if randomise:
			random.shuffle(data)
		nums=[get_file_nums(os.path.basename(strg))[0] for strg in self.filenames]
		if len(nums)==0: #this sets the next value for the filename
			idx=-1
		else:
			idx=max(nums)

		datachunks=chunks(data,n_per_file,trim=False)
		for d in datachunks:
			idx+=1
			X, Y=list(zip(*d))
			filename=os.path.join(self.folder,f"{idx}.json")
			json_tricks.dump([np.array(X),np.array(Y)],filename)

	def get_testset_batch(self,batchsize=None):
		#loads a batch from the testset from the disk
		if batchsize==None: # in case you want more data at a time.
			batchsize=self.batchsize
		###gets one batch of data.
		if not self.testset_started:
			self.testset_started=True
			self.testfilesremaining = self.testfiles.copy()  # as files are loaded they are removed from this list so we dont load them twice in one epoch
			random.shuffle(self.testfilesremaining) #todo: can reduce memory size by removing the folder from this list and adding it back when loading. Also not sure if I care about shuffling the test set. it means each repeat a different amount will be given

		outoffilesflag=False
		finished=False
		batchx,batchy=[],[]

		while len(batchx)<batchsize: ##then need to add more data to the pool
			if len(self.testfilesremaining) == 0:
				outoffilesflag=True
				break
			filename = self.testfilesremaining.pop(-1)
			X,Y=json_tricks.load(filename)
			batchx.extend(X)
			batchy.extend(Y)
		if outoffilesflag:
			if len(batchx)<batchsize: ##then we are out of files to load and don't have enough data for 1 full batch.
				finished=True   #we will return whatever we can but indicate that this epoch is finished.
				self.testset_started=False #this is so the next time this fn is called we start a new epoch


		X=np.array(batchx)
		Y = np.array(batchy)

		return X,Y,finished

	def get_batch(self):
		self.batchcounter+=1
		###gets one batch of data.
		if not self.epochstarted:
			self.epochstarted=True
			#self.filenames = glob.glob(f"{self.folder}/*.json")  # if you want to generate data while you are learning then you could uncomment this, however your test set data may be stuffed up.
			self.filenamesremaining = self.filenames.copy()  # as files are loaded they are removed from this list so we dont load them twice in one epoch
			random.shuffle(self.filenamesremaining) #todo: can reduce memory size by removing the folder from this list
			self.batchcounter=0

		outoffilesflag=False
		finished=False
		while len(self.storedX)<self.datapoolsize: ##then need to add more data to the pool
			if len(self.filenamesremaining) == 0:
				outoffilesflag=True
				break
			filename = self.filenamesremaining.pop(-1)
			X,Y=json_tricks.load(filename)
			self.storedX.extend(X)
			self.storedY.extend(Y)
		if outoffilesflag:
			if len(self.storedX)<self.batchsize: ##then we are out of files to load and don't have enough data for 1 full batch.
				finished=True   #we will return whatever we can but indicate that this epoch is finished.
				self.epochstarted=False #this is so the next time this fn is called we start a new epoch
				self.epochcounter += 1
		combinedD=list(zip(self.storedX,self.storedY))
		random.shuffle(combinedD)
		n=min(self.batchsize,len(combinedD))
		thisdata=combinedD[:n]
		keptdata = combinedD[n:]
		X,Y=list(zip(*thisdata))
		if len(keptdata)==0:
			self.storedX, self.storedY=[],[]
		else:
			self.storedX,self.storedY=list(zip(*keptdata))
			self.storedX, self.storedY=list(self.storedX),list(self.storedY)
		#idxes=np.arange(0,len(self.storedX))
		#batchidxes=np.random.choice(idxes,min(self.batchsize,len(self.storedX)),replace=False)

		#X=np.array(self.storedX)[batchidxes]
		#Y = np.array(self.storedY)[batchidxes]
		#self.storedX=list(np.delete(self.storedX, batchidxes))
		#self.storedY=list(np.delete(self.storedY, batchidxes))


		return np.array(X),np.array(Y),finished

class DataManagerNoPool():
	"""
	This class is used when all the data is held in memory.


	"""
	def __init__(self,folder,testsplit=.2,batchsize=600,datapoolsize=None):
		"""
		helper functions to manage data for ANN training which is too big to fit into memory.
		:param folder: the folder where the data is to be stored
		:param testsplit: the proportion of files to be held back from the dataset for testing
		:param batchsize: the size of each batch
		:param datapoolsize: Not used. Just here for consistency.
		"""
		#testsplit is the proportion of data to hold back as the test set.
		#note that it does not hold back exactly the data instead it simply holds back that proportion of files.
		#There could be issues with correlation of data if your files are highly correlated.
		#
		self.testsplit=0.2
		self.folder=folder
		os.makedirs(folder, exist_ok=True)
		self.filenames = glob.glob(f"{folder}/*.json")  # get current file names.
		n=int(testsplit*len(self.filenames))
		idxes=np.arange(0,len(self.filenames))
		testidxes=np.random.choice(idxes,n,replace=False)
		self.testfiles=list(np.array(self.filenames)[testidxes])
		self.filenames=list(np.delete(self.filenames, testidxes))
		self.filenamesremaining=[]  #as files are loaded they are removed from this list so we dont load them twice in one epoch
		self.batchsize=batchsize
		self.epochstarted=False #is True after the first batch is done.
		self.storedX=[]
		self.storedY=[]
		self.testX=[]
		self.testY=[]
		self.lastfile=False
		self.batchcounter=0  #a counter that keeps track of how many batches have been done
		self.epochcounter=0
		self.testset_started=False
		self.loaded_data=False
		self.loaded_test_data=False
		self.pick_idxes=[]  #these are the indexes for retrieving the data
							# - instead of randomising the whole list, we just shuffle these numbers.
							# - also we pop the numbers off the list and recreate it when its empty.
		self.pick_T_idxes=[]

	def saveData(self,X_true,Y_true,n_per_file=10,randomise=True,folder=None):
		"""
		Saves ground truth data conveniently.
		:param X_true,Y_true: X_true is an n-D numpy array/tensor. Y-true is an n vector representing the ground truth value for X-true
		:param filename: The folder to save the data
		:param n_per_file=10: how many samples to save per file. Using 1 means that each example will be in its own file.
				A smaller number is efficient when randomising data to train but will be slower to load.
				Larger numbers will be more efficient to load, but may mean that randomisations are not truly random cause the same n datapoints may always occur together.
				If all the data fits into memory at once then you can overcome these points. If however you need to load data then the smaller the n the better.
		:param randomise=True: this randomises the datapool before selecting data to be stored in a file - it will decrease the chance of data being correlated within the same file.
		:return:
		"""

		data=list(zip(X_true,Y_true))
		if randomise:
			random.shuffle(data)
		nums=[get_file_nums(os.path.basename(strg))[0] for strg in self.filenames]
		if len(nums)==0: #this sets the next value for the filename
			idx=-1
		else:
			idx=max(nums)

		datachunks=chunks(data,n_per_file,trim=False)
		for d in datachunks:
			idx+=1
			X, Y=list(zip(*d))
			filename=os.path.join(self.folder,f"{idx}.json")
			json_tricks.dump([np.array(X),np.array(Y)],filename)

	def get_testset_batch(self,batchsize=None):
		if batchsize==None:
			batchsize=self.batchsize
		###gets one batch of data.
		if not self.loaded_test_data:
			self.load_test_data()
			self.loaded_test_data = True

		if not self.testset_started:
			self.testset_started = True
			self.pick_T_idxes = list(range(len(self.testY)))
			random.shuffle(self.pick_T_idxes)

		finished = False
		n = min(batchsize, len(self.pick_T_idxes))
		idxes = self.pick_T_idxes[:n]
		self.pick_T_idxes = self.pick_T_idxes[n:]
		X = self.testX[idxes]
		Y = self.testY[idxes]
		if len(self.pick_T_idxes) == 0:
			self.testset_started = False  # this is so the next time this fn is called we start a new epoch
			finished = True  # last batch. Might be smaller size than BS


		return np.array(X), np.array(Y), finished


	def load_test_data(self,report_every_perc=10):
		###Loads the data to memory
		####report_every_perc sets reporting of loading every n percent because it might be a slow process.
		report_every=int(report_every_perc/100*len(self.testfiles))

		print(f"loading Data")
		for idx,filename in enumerate(self.testfiles):
			X,Y=json_tricks.load(filename)
			try:
				self.testX.extend(X)
				self.testY.extend(Y)
				if idx%report_every==0:
					print(f"nfiles={idx} \t nX={len(self.testX)} \t {int(100*idx/len(self.testfiles))}% done",flush=True)
			except:
				#probably out of memory.
				print(f"ERROR STORING DATA for {filename}. {int(idx/len(self.testfiles))}% done")
				print(f"{len(self.testX),len(self.testY)} records in X,Y") #if they are the same then X caused the error. If not then Y did.
				mem() ##print memory stats
				print(f"\n",flush=True)
				raise
		print(f"nfiles={idx} \t nX={len(self.testX)} \t {int(100 * idx / len(self.testfiles))}% done", flush=True)

		self.testX=np.array(self.testX)
		self.testY = np.array(self.testY)

	def load_data(self,report_every_perc=10):
		###Loads the data to memory
		####report_every_perc sets reporting of loading every n percent because it might be a slow process.
		report_every=int(report_every_perc/100*len(self.filenames))
		self.filenames = glob.glob(f"{self.folder}/*.json")  # get current file names.

		print(f"loading Data")
		for idx,filename in enumerate(self.filenames):
			X,Y=json_tricks.load(filename)
			try:
				self.storedX.extend(X)
				self.storedY.extend(Y)
				if idx%report_every==0:
					print(f"nfiles={idx} \t nX={len(self.storedX)} \t {int(100*idx/len(self.filenames))}% done",flush=True)
			except:
				#probably out of memory.
				print(f"ERROR STORING DATA for {filename}. {int(idx/len(self.filenames))}% done")
				print(f"{len(self.storedX),len(self.storedY)} records in X,Y") #if they are the same then X caused the error. If not then Y did.
				mem() ##print memory stats
				print(f"\n",flush=True)
				raise
		print(f"nfiles={idx} \t nX={len(self.storedX)} \t {int(100 * idx / len(self.filenames))}% done", flush=True)
		self.pick_idxes=list(range(len(self.storedY)))
		self.storedX=np.array(self.storedX)
		self.storedY = np.array(self.storedY)

	def get_batch(self):
		###gets one batch of data.
		if not self.loaded_data:
			self.load_data()
			self.loaded_data=True

		if not self.epochstarted:
			self.epochstarted=True
			self.batchcounter=0
			self.pick_idxes=list(range(len(self.storedY)))
			random.shuffle(self.pick_idxes)
		finished=False
		n=min(self.batchsize,len(self.pick_idxes))
		idxes=self.pick_idxes[:n]
		self.pick_idxes=self.pick_idxes[n:]
		X=self.storedX[idxes]
		Y=self.storedY[idxes]
		if len(self.pick_idxes)==0:
			self.epochstarted = False  # this is so the next time this fn is called we start a new epoch
			finished=True  #last batch. Might be smaller size than BS
			self.epochcounter+=1

		self.batchcounter+=1

		return np.array(X),np.array(Y),finished

def example_create_training_DataManager():
	m = MNEDataset(batch_step_s=1.5, batch_window_s=3)
	saveevery = 500
	folder = os.path.abspath("test")
	dm = DataManager(folder)  ###
	i = 0
	filecount = 0
	requiredl = 750
	ekgchan=None
	##find EKG channel
	for idx,chan in enumerate(m.channels):
		if re.compile("EKG").search(chan):
			ekgchan=idx
			break

	Xs=[]
	Ys=[]
	while True:
		i = i + 1
		eegchan = None
		while eegchan==None:
			thisEEG = random.choice(m.EEG_Labels)
			eegchan = m.eeglabelsdict[thisEEG]


		# ekgchan = random.choice(list(ekglabelsdict.values()))

		data, times, done = m.get_next_batch()
		if data.shape[0]==0:
			pass
		if len(times) == requiredl:
			fft_val, freqs = m.get_fft(data)
			ekg_data = fft_val[ekgchan]
			eeg_data = fft_val[eegchan]
			ekg_mag = np.abs(ekg_data)
			ekg_angle = np.angle(ekg_data)
			eeg_mag = np.abs(eeg_data)
			eeg_angle = np.angle(eeg_data)


			eegX = np.array(list(zip(eeg_mag, eeg_angle)))
			ekgX = np.array(list(zip(ekg_mag, ekg_angle)))

			if len(np.array(eegX).shape)!=2 or np.array(eegX).shape[0]!=376:
					print(f"Unusual EEG Shape")
			if len(np.array(ekgX).shape)!=2 or np.array(ekgX).shape[0]!=376:
					print(f"Unusual EKG Shape")
			Xs.append(eegX) #make sure you double check the shape of the data
			Ys.append([1])
			Xs.append(ekgX)
			Ys.append([0])
			if i % saveevery == 0:
				dm.saveData(X_true=np.array(Xs), Y_true=np.array(Ys), n_per_file=300)  #
				Xs = []
				Ys = []
		else:
			# fft_val2,freqs2=m.get_fft(data)
			dm.saveData(X_true=np.array(Xs), Y_true=np.array(Ys), n_per_file=300)  #
			Xs = []
			Ys = []
		if done:
			filecount += 1
			for idx, chan in enumerate(m.channels):
				if re.compile("EKG").search(chan):
					ekgchan = idx
					break

			if filecount >= 2:
				break
			else:
				done = False
from sklearn.neural_network import MLPRegressor

def example_read_training_DataManager():
	#m = MNEDataset(batch_step_s=1.5, batch_window_s=3)
	#saveevery = 500
	folder = os.path.abspath("test")
	dm = DataManager(folder)  ###
	##find EKG channel
	X_test,Y_test,finished=dm.get_testset_batch(1000) #just get 1000 held back examples
	X_test = X_test.reshape((X_test.shape[0], -1))
	reg = MLPRegressor(hidden_layer_sizes=[40, 5, 5], learning_rate="adaptive", random_state=1, max_iter=1,shuffle=False)  # takes 15 epochs to get 0.99999
	breakthresh=.999
	nepochs=5000
	best=0
	fin=False
	for i in range(nepochs):
		done=False
		while not done:
			X,Y,done=dm.get_batch()
			if not done: #note - just for convenience wont do the last batch because it might be smaller than batchsize.
				X = X.reshape((X.shape[0], -1))
				Y = Y.reshape((Y.shape[0]))
				reg.partial_fit(X,Y) #do one step

			score=reg.score(X_test,Y_test)

			print(f"\r step: {dm.batchcounter} \t score:{score} ",end="")
			if score>breakthresh:
				print(f"\n Finished")
				fin=True
				break
		if score>best:
			best=score
			isbest=True
		else:
			isbest=False

		print(f"\r------ epoch: {dm.epochcounter} \t {[' ','*'][isbest]}score:{score} ---------------------")
		if fin==True:
			break

if __name__ == '__main__':
	# how to create training examples from an edf
	#example_create_training_DataManager()
	#example loading examples run example_create_training_DataManager() first to create the training set from EEG
	#example_read_training_NOPOOL_DataManager()
	#example_read_training_DataManager()

	##example how to use MNE
	m = MNEDataset(batch_step_s=29, batch_window_s=30)

	folder=os.path.abspath("test")
	#m.saveData(X_true,Y_true,folder)

	data, times, last = m.get_next_batch()
	freq = 1 / times[1]

	###Calling functions directly
	showdata_s, time_s,last = m.get_data_samples(0, m.samplerate)  # this should be 1 seconds worth of samples
	showdata_t, time2_t, last = m.get_data_time(0, 1 )  # this is 1 second worth of samples should be same as above

	###Cycling through the data.
	counter = 0
	while (not last):
		data, times, last = m.get_next_batch()
		counter += 1
		if last:
			print(f"{times[-1] / 60}s")
			print(f"{counter} steps")

	showdataNP=np.array(showdata_s)
	showdataNP=cv.normalize(showdataNP,  showdata_s, 0, 255, cv.NORM_MINMAX)
	plt.imshow(showdataNP,cmap="gray",interpolation=None)
	img = Image.fromarray(showdataNP,'L')

	img.show()
	plt.show()

	# Use the plotting tools to show the eeg
	mne.viz.plot_raw(m.edfdata)

	print("Done")
